<?php namespace Ayedev\Bot\Messenger\Event;

use Ayedev\Bot\Messenger\IFace\EventInterface;
use Ayedev\Bot\Messenger\Impl\AbstractMessage;
use Symfony\Component\EventDispatcher\Event;

class ChatEvent extends Event
{
    //  Event Name for Request
    const REQUEST = 'chat.received';

    //  Event Name for Response
    const RESPONSE = 'chat.sent';

    /** @var string $_sender_id */
    private $_sender_id;

    /** @var string $_receiver_id */
    private $_receiver_id;

    /** @var string $_page_id */
    private $_page_id;

    /** @var string $_session_id */
    private $_session_id;

    /** @var int $_timestamp */
    private $_timestamp;

    /** @var bool $_to_user */
    private $_to_user = false;

    /** @var string $_message */
    private $_message;

    /** @var string $_stage */
    private $_stage;


    /**
     * ChatEvent constructor.
     *
     * @param $sender_id
     * @param $receiver_id
     * @param $page_id
     * @param $session_id
     * @param $timestamp
     * @param $message
     * @param bool $to_user
     * @param null $stage
     */
    public function __construct( $sender_id, $receiver_id, $page_id, $session_id, $timestamp, $message, $to_user = false, $stage = null )
    {
        //  Store
        $this->_sender_id = $sender_id;
        $this->_receiver_id = $receiver_id;
        $this->_page_id = $page_id;
        $this->_session_id = $session_id;
        $this->_timestamp = $timestamp;
        $this->_message = $message;
        $this->_to_user = $to_user;
        $this->_stage = $stage;
    }

    /**
     * Get Sender ID
     *
     * @return string
     */
    public function getSenderID()
    {
        //  Return
        return $this->_sender_id;
    }

    /**
     * Get Receiver ID
     *
     * @return string
     */
    public function getReceiverID()
    {
        //  Return
        return $this->_receiver_id;
    }

    /**
     * Get Page ID
     *
     * @return string
     */
    public function getPageID()
    {
        //  Return
        return $this->_page_id;
    }

    /**
     * Get Session ID
     *
     * @return string
     */
    public function getSessionID()
    {
        //  Return
        return $this->_session_id;
    }

    /**
     * Get Timestamp
     *
     * @return int
     */
    public function getTimestamp()
    {
        //  Return
        return $this->_timestamp;
    }

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage()
    {
        //  Return
        return $this->_message;
    }

    /**
     * Check is for user
     *
     * @return bool
     */
    public function isForUser()
    {
        //  Return
        return $this->_to_user;
    }

    /**
     * Check is for admin
     *
     * @return bool
     */
    public function isForAdmin()
    {
        //  Return
        return !$this->isForUser();
    }

    /**
     * Get Stage
     *
     * @return string
     */
    public function getStage()
    {
        //  Return
        return $this->_stage;
    }

    /**
     * Get Related Event
     *
     * @return Event|null
     */
    public function getRelatedEvent()
    {
        //  Return
        return mManager()->getWebhook()->eventForSession( $this->getSessionID() );
    }


    /**
     * Dispatch Request Event
     *
     * @param EventInterface $event
     * @param string $stage
     */
    public static function dispatchRequestEvent( EventInterface $event, $stage = 'default' )
    {
        //  Dispatch Event
        mManager()->dispatch( static::REQUEST, new static( $event->getSenderID(), $event->getRecipientID(), $event->getPageID(), $event->getSessionID(), $event->getTimestamp(), $event->toString(), false, $stage ) );
    }

    /**
     * Dispatch Response Event
     *
     * @param EventInterface $event
     * @param AbstractMessage $message
     * @param string $stage
     */
    public static function dispatchResponseEvent( EventInterface $event, AbstractMessage $message, $stage = 'default' )
    {
        //  Dispatch Event
        mManager()->dispatch( static::RESPONSE, new static( $event->getRecipientID(), $event->getSenderID(), $event->getPageID(), $event->getSessionID(), time(), $message->toString(), true, $stage ) );
    }
}