<?php namespace Ayedev\Bot\Messenger\Core;

use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Ayedev\Bot\Messenger\Exception\ApiException;
use Ayedev\Bot\Messenger\Impl\AbstractSkeleton;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class ApiCall
 */
class ApiCall extends AbstractSkeleton
{
    /**
     * Request default timeout
     */
    const DEFAULT_TIMEOUT = 60;

    /**
     * Request default connect timeout
     */
    const DEFAULT_CONNECT_TIMEOUT = 10;

    /**
     * Request default file upload timeout
     */
    const DEFAULT_FILE_UPLOAD_TIMEOUT = 3600;


    /** @var string $_version */
    public $_version;

    /** @var string $_api_url */
    public $_api_url;

    /** @var string $_token */
    private $_token;

    /** @var ResponseInterface|null $_response */
    private $_response;

    /** @var Client $_client */
    protected $_client;

    /** @var bool $_verify */
    protected $_verify = true;

    /** @var bool $_use_accessToken */
    protected $_use_accessToken = true;

    /** @var bool $_defaultVerify */
    private static $_defaultVerify = true;


    /**
     * Return Instance
     *
     * @param array $args
     * @return ApiCall
     */
    public static function instance( ...$args )
    {
        //  Return
        return parent::instance( ...$args );
    }

    /**
     * Set Default cUrl Verification State
     * 
     * @param bool $flag
     */
    public static function setDefaultCurlVerification( $flag )
    {
        //  Store
        static::$_defaultVerify = $flag;
    }


    /**
     * ApiCall constructor.
     *
     * @param null $token
     * @param null $client
     * @param string $api_url
     * @param string $version
     */
    public function __construct( $token = null, $client = null, $api_url = null, $version = null )
    {
        //  Store Api Details
        if( $api_url )  $this->setApiUrl( $api_url );
        if( $version )  $this->setVersion( $version );

        //  Store Token
        if( $token )    $this->setToken( $token );

        //  Set Verification
        $this->_verify = static::$_defaultVerify;

        //  Store Client
        $this->setClient( $client ?: $this->_defaultClient() );
    }


    /**
     * Enable cUrl Verification
     *
     * @return $this
     */
    public function enableCurlVerification()
    {
        //  Enable
        $this->_verify = true;

        //  Return
        return $this;
    }

    /**
     * Disable cUrl Verification
     *
     * @return $this
     */
    public function disableCurlVerification()
    {
        //  Disable
        $this->_verify = false;

        //  Return
        return $this;
    }


    /**
     * Enable Use Access Token
     *
     * @return $this
     */
    public function enableUseAccessToken()
    {
        //  Enable
        $this->_use_accessToken = true;

        //  Return
        return $this;
    }

    /**
     * Disable Use Access Token
     *
     * @return $this
     */
    public function disableUseAccessToken()
    {
        //  Disable
        $this->_use_accessToken = false;

        //  Return
        return $this;
    }


    /**
     * Set Version
     *
     * @param $version
     * @return $this
     */
    public function setVersion( $version )
    {
        //  Store
        $this->_version = $version;

        //  Return
        return $this;
    }

    /**
     * Get Version
     *
     * @return string
     */
    public function getVersion()
    {
        //  Return
        return $this->_version;
    }


    /**
     * Set Api URL
     *
     * @param $api_url
     * @return $this
     */
    public function setApiUrl( $api_url )
    {
        //  Store
        $this->_api_url = $api_url;

        //  Return
        return $this;
    }

    /**
     * Get Api URL
     *
     * @return string
     */
    public function getApiUrl()
    {
        //  Return
        return $this->_api_url;
    }


    /**
     * Create default client
     *
     * @return \GuzzleHttp\Client
     */
    private function _defaultClient()
    {
        //  Return
        return new Client( array (
            'base_uri' => $this->_api_url,
            'timeout' => static::DEFAULT_TIMEOUT,
            'connect_timeout' => static::DEFAULT_CONNECT_TIMEOUT,
            'verify' => $this->_verify
        ) );
    }

    /**
     * Set Client
     *
     * @param $client
     * @return $this
     */
    public function setClient( $client )
    {
        //  Store
        $this->_client = $client;

        //  Return
        return $this;
    }

    /**
     * Get Client
     *
     * @return Client
     */
    public function getClient()
    {
        //  Return
        return $this->_client;
    }


    /**
     * Set Access Token
     *
     * @param $token
     * @return $this
     */
    public function setToken( $token )
    {
        //  Store
        $this->_token = $token;

        //  Return
        return $this;
    }

    /**
     * With Token (ALIAS)
     * s
     * @param $token
     * @return $this
     */
    public static function withToken( $token )
    {
        //  Return
        return self::call( 'setToken', $token );
    }

    /**
     * Get Access Token
     *
     * @return string
     */
    public function getToken()
    {
        //  Return
        return $this->_token;
    }


    /**
     * Send GET Request
     *
     * @param string $uri
     * @param array $params
     *
     * @return $this
     */
    public function get( $uri, array $params = [] )
    {
        //  Return
        return $this->send( 'GET', $uri, null, $params );
    }

    /**
     * Send POST Request
     *
     * @param string $uri
     * @param mixed $body
     *
     * @return $this
     */
    public function post( $uri, $body = null )
    {
        //  Return
        return $this->send( 'POST', $uri, $body );
    }

    /**
     * Send PUT Request
     *
     * @param string $uri
     * @param mixed $data
     *
     * @return $this
     */
    public function put( $uri, $data = null )
    {
        //  Return
        return $this->send( 'PUT', $uri, $data );
    }

    /**
     * Send DELETE Request
     *
     * @param string $uri
     *
     * @param null $data
     * @return $this
     */
    public function delete( $uri, $data = null )
    {
        //  Return
        return $this->send( 'DELETE', $uri, $data );
    }


    /**
     * @param string $method
     * @param string $uri
     * @param mixed $body
     * @param array $query
     * @param array $headers
     * @param array $options
     *
     * @return $this
     *
     * @throws ApiException
     */
    public function send( $method, $uri, $body = null, array $query = [], array $headers = [], array $options = [] )
    {
        //  Enhance Options
        $options = $this->enhanceOptions( $body, $query, $headers, $options );

        //  Set Verify Option
        $options['verify'] = $this->_verify;

        //  Try
        try
        {
            //  Get Response
            $this->_response = $this->getClient()->request( $method, ( $this->_version ? '/' . $this->_version : '' ) . $uri, $options );
        }
        catch( GuzzleException $e )
        {
            //  Error Response
            $errResponse = $e->getResponse();

            //  Exception
            $exception = new ApiException( $e->getMessage(), $e->getCode() );

            //  Set Main Exception
            $exception->setMainException( $e );

            //  Check
            if( $errResponse && $errResponse->getBody() )
            {
                //  Set Response
                $exception->setResponse( json_decode( (string) $errResponse->getBody()->getContents(), true ) );

                //  Check
                if( $headError = $errResponse->getHeader( 'WWW-Authenticate' ) )
                {
                    //  Set Head Error
                    $exception->setHeadError( implode( "\n", $headError ) );
                }
            }

            //  Throw Messenger Exception
            throw $exception;
        }

        //  Validate Response
        $this->_validateResponse( $this->_response );

        //  Return
        return $this;
    }

    /**
     * @param ResponseInterface $response
     *
     * @return $this
     * @throws ApiException
     */
    private function _validateResponse( ResponseInterface $response )
    {
        //  Check for Status Code
        if( $response && $response->getStatusCode() !== 200 )
        {
            //  Get Response Date
            $responseData = json_decode( (string) $response->getBody(), true );

            //  Get Code
            $code = isset($responseData['error']['code']) ? $responseData['error']['code'] : 0;

            //  Get Message
            $message = isset($responseData['error']['message']) ? $responseData['error']['message'] : $response->getReasonPhrase();

            //  Throw Exception
            throw ( new ApiException( $message, $code ) )->setResponse( $responseData );
        }

        //  Return
        return $this;
    }

    /**
     * Enhance Options
     *
     * @param null $body
     * @param array $query
     * @param array $headers
     * @param array $options
     * @return array
     */
    private function enhanceOptions( $body = null, array $query = [], array $headers = [], array $options = [] )
    {
        //  Check for Access Token
        if(  $this->_use_accessToken && !isset( $query['access_token'] ) && $this->getToken() )
        {
            //  Set Access Token
            $query['access_token'] = $this->getToken();
        }

        //  Check for Body
        if( !empty( $body ) && is_array( $body ) )
        {
            //  Encode Body
            $body = json_encode( $body );

            //  Set Content Type
            $headers['Content-Type'] = 'application/json';
        }

        //  Update Options
        $options[RequestOptions::BODY] = $body;
        $options[RequestOptions::QUERY] = $query;
        $options[RequestOptions::HEADERS] = $headers;

        //  Return
        return $options;
    }

    /**
     * Get Response
     *
     * @return null|ResponseInterface
     */
    public function getResponse()
    {
        //  Return
        return $this->_response;
    }

    /**
     * Get Decoded Response
     *
     * @return array
     */
    public function getResponseDecoded()
    {
        //  Return
        return ( $this->_response ? json_decode( (string) $this->_response->getBody(), true ) : null );
    }
}