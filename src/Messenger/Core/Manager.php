<?php namespace Ayedev\Bot\Messenger\Core;

use Ayedev\Bot\AI\IFace\AIInterface;
use Ayedev\Bot\Messenger\IFace\MessengerInterface;
use Ayedev\Bot\Messenger\IFace\WebhookInterface;
use Ayedev\Bot\Messenger\Impl\AbstractSkeleton;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class Manager extends AbstractSkeleton
{
    //  Constants
    const INS_WEBHOOK = 'webhook';              //  Webhook
    const INS_DISPATCHER = 'dispatcher';        //  Dispatcher
    const INS_AI = 'ai';                        //  AI
    const INS_MESSENGER = 'messenger';          //  Messenger
    const DATA_DIR = 'data_dir';                //  Data Directory
    const PLATFORM = 'platform';                //  Running Service
    const DEBUG = 'debug';                      //  Debug


    /** @var array $_instances */
    protected $_instances;


    /**
     * Get the Instance
     *
     * @param array $args
     * @return Manager
     */
    public static function instance( ...$args )
    {
        //  Return
        return parent::instance( ...$args );
    }


    /**
     * Manager constructor.
     *
     * @param $platform
     * @param WebhookInterface $webhook
     * @param MessengerInterface $messenger
     * @param $data_dir
     * @param AIInterface|null $ai
     * @param EventDispatcherInterface|null $dispatcher
     * @param bool $debug
     */
    public function __construct( $platform, WebhookInterface $webhook, MessengerInterface $messenger, $data_dir, AIInterface $ai = null, EventDispatcherInterface $dispatcher = null, $debug = false )
    {
        //  Store Webhook
        $this->setWebhook( $webhook );

        //  Store Messenger
        $this->setMessenger( $messenger );

        //  Store Dispatcher
        $this->setDispatcher( $dispatcher ?: new EventDispatcher );

        //  Store Platform
        $this->setPlatform( $platform );

        //  Store Data Dir
        $this->setDataDir( $data_dir );

        //  Store AI
        if( $ai )   $this->setAI( $ai );

        //  Set Debug
        $this->setDebug( $debug );
    }

    /**
     * Assign Instance
     *
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function assignInstance( $name, $value )
    {
        //  Store
        $this->_instances[$name] = $value;

        //  Return
        return $this;
    }

    /**
     * Get Assigned Instance
     *
     * @param string $name
     * @param null $def
     * @return mixed
     */
    public function readInstance( $name, $def = null )
    {
        //  Return
        return ( isset( $this->_instances[$name] ) ? $this->_instances[$name] : $def );
    }


    /**
     * Enable Debug Mode
     * 
     * @return $this
     */
    public function enableDebug()
    {
        //  Return
        return $this->setDebug( true );
    }

    /**
     * Disable Debug Mode
     * 
     * @return $this
     */
    public function disableDebug()
    {
        //  Return
        return $this->setDebug( false );
    }

    /**
     * Set Debug Mode
     * 
     * @param int|bool $flag
     * @return $this
     */
    public function setDebug( $flag )
    {
        //  Return
        return $this->assignInstance( static::DEBUG, (bool)( (int) $flag ) );
    }

    /**
     * Check Debug is Enabled
     * 
     * @return bool
     */
    public function isDebugEnabled()
    {
        //  Return
        return (bool)( (int)$this->readInstance( static::DEBUG, 0 ) );
    }

    /**
     * Set Webhook
     *
     * @param WebhookInterface $webhook
     * @return $this
     */
    public function setWebhook( WebhookInterface $webhook )
    {
        //  Return
        return $this->assignInstance( static::INS_WEBHOOK, $webhook );
    }

    /**
     * Get Webhook
     *
     * @return WebhookInterface
     */
    public function getWebhook()
    {
        //  Return
        return $this->readInstance( static::INS_WEBHOOK );
    }

    /**
     * Set Messenger
     *
     * @param MessengerInterface $messenger
     * @return $this
     */
    public function setMessenger( MessengerInterface $messenger )
    {
        //  Return
        return $this->assignInstance( static::INS_MESSENGER, $messenger );
    }

    /**
     * Get Messenger
     *
     * @return MessengerInterface
     */
    public function getMessenger()
    {
        //  Return
        return $this->readInstance( static::INS_MESSENGER );
    }

    /**
     * Set Dispatcher
     *
     * @param EventDispatcherInterface $dispatcher
     * @return $this
     */
    public function setDispatcher( EventDispatcherInterface $dispatcher )
    {
        //  Return
        return $this->assignInstance( static::INS_DISPATCHER, $dispatcher );
    }

    /**
     * Get Dispatcher
     *
     * @return EventDispatcherInterface
     */
    public function getDispatcher()
    {
        //  Return
        return $this->readInstance( static::INS_DISPATCHER );
    }

    /**
     * Set AI
     *
     * @param AIInterface $ai
     * @return $this
     */
    public function setAI( AIInterface $ai )
    {
        //  Return
        return $this->assignInstance( static::INS_AI, $ai );
    }

    /**
     * Get AI
     *
     * @return AIInterface
     */
    public function getAI()
    {
        //  Return
        return $this->readInstance( static::INS_AI );
    }

    /**
     * Set Data Dir
     *
     * @param string $dir
     * @return $this
     */
    public function setDataDir( $dir )
    {
        //  Return
        return $this->assignInstance( static::DATA_DIR, $dir );
    }

    /**
     * Get Data Directory
     *
     * @return string
     */
    public function getDataDir()
    {
        //  Return
        return $this->readInstance( static::DATA_DIR, '/' );
    }

    /**
     * Set Platform
     *
     * @param string $platform
     * @return $this
     */
    public function setPlatform( $platform )
    {
        //  Return
        return $this->assignInstance( static::PLATFORM, $platform );
    }

    /**
     * Get Platform
     *
     * @return string
     */
    public function getPlatform()
    {
        //  Return
        return $this->readInstance( static::PLATFORM );
    }


    /**
     * Dispatch Events
     *
     * @param array ...$args
     * @return \Symfony\Component\EventDispatcher\Event
     */
    public function dispatch( ...$args )
    {
        //  Run Dispatcher
        return $this->getDispatcher()->dispatch( ...$args );
    }

    /**
     * Add Subscriber
     *
     * @param array ...$args
     * @return \Symfony\Component\EventDispatcher\Event
     */
    public function addSubscriber( ...$args )
    {
        //  Run Dispatcher
        return $this->getDispatcher()->addSubscriber( ...$args );
    }

    /**
     * Add Listeners
     *
     * @param array ...$args
     * @return \Symfony\Component\EventDispatcher\Event
     */
    public function addListener( ...$args )
    {
        //  Run Dispatcher
        return $this->getDispatcher()->addListener( ...$args );
    }


    /**
     * Get Data Path
     *
     * @param $file
     * @param null $dir
     * @return string
     */
    public function getDataPath( $file = null, $dir = null )
    {
        //  File Path
        $filePath = $this->getDataDir() . $this->getPlatform() . DIRECTORY_SEPARATOR . ( $dir ? $dir . DIRECTORY_SEPARATOR : '' ) . ( $file ?: '' );

        //  Check
        if( pathinfo( $filePath, PATHINFO_EXTENSION ) )
        {
            //  Parent Dir
            $parentDir = dirname( $filePath );

            //  Check
            if( !file_exists( $parentDir ) )    @mkdir( $parentDir, 0775, true );
        }
        else
        {
            //  Check
            if( !file_exists( $filePath ) )    @mkdir( $filePath, 0775, true );
        }

        //  Return
        return $filePath;
    }


    /**
     * Temporary Work Setup
     *
     * @param callable $callback
     * @param array ...$args
     * @return $this
     */
    public function runIsolated( callable $callback, ...$args )
    {
        //  Get Current Instances
        $instances = $this->_instances;

        //  Check
        if( $callback instanceof \Closure )
        {
            //  Bind the Callback
            $callback = $callback->bindTo( $this );
        }

        //  Run Callback
        call_user_func_array( $callback, $args );

        //  Revert
        $this->_instances = $instances;

        //  Return
        return $this;
    }

    /**
     * Work with Stored Instance
     *
     * @param $name
     * @param $callback
     * @param array ...$args
     * @return $this
     */
    public function with( $name, $callback, ...$args )
    {
        //  Get Object
        $object = $this->readInstance( $name );

        //  Check
        if( $object )
        {
            //  Check
            if( $callback instanceof \Closure )
            {
                //  Bind the Callback
                $callback = $callback->bindTo( $this );
            }

            //  Prepend to Args
            array_splice( $args, 0, 0, array( $object ) );

            //  Do Callback
            call_user_func_array( $callback, $args );
        }

        //  Return
        return $this;
    }
}