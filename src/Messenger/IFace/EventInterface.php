<?php namespace Ayedev\Bot\Messenger\IFace;

use Ayedev\Bot\AI\IFace\AIResponseInterface;
use Ayedev\Bot\Messenger\Exception\GeneralException;
use Ayedev\Bot\Messenger\Impl\AbstractEntry;

interface EventInterface
{
    //  Raw Data
    const RAW_DATA = 'raw';

    //  Message Echo
    const MESSAGE_ECHO = 'message.echo';

    //  Message Received
    const MESSAGE_RECEIVED = 'message.received';

    //  Message Postback
    const MESSAGE_POSTBACK = 'message.postback';

    //  Message Delivery
    const MESSAGE_DELIVERY = 'message.delivery';

    //  Message Read
    const MESSAGE_READ = 'message.read';

    //  Location Received
    const LOCATION_RECEIVED = 'location.received';


    /**
     * Check Event Stopped
     *
     * @return bool
     */
    public function isPropagationStopped();

    /**
     * Stop Event
     */
    public function stopPropagation();

    /**
     * Check Event Prevented
     *
     * @return bool
     */
    public function isDefaultPrevented();

    /**
     * Prevent Default
     *
     * @return $this
     */
    public function preventDefault();

    /**
     * Attach Entry
     *
     * @param AbstractEntry $entry
     * @return $this
     */
    public function attachEntry( AbstractEntry $entry );

    /**
     * Attach Response
     *
     * @param AIResponseInterface $response
     * @return $this
     */
    public function attachResponse( AIResponseInterface $response );

    /**
     * Get Entry
     *
     * @return AbstractEntry
     */
    public function getEntry();

    /**
     * Get Response
     *
     * @return AIResponseInterface
     */
    public function getResponse();

    /**
     * Check has response
     *
     * @return bool
     */
    public function hasResponse();

    /**
     * Get Sender
     *
     * @return array
     */
    public function getSender();

    /**
     * Get Sender ID
     *
     * @return string
     */
    public function getSenderID();

    /**
     * Get Recipient
     *
     * @return array
     */
    public function getRecipient();

    /**
     * Get Recipient ID
     *
     * @return string
     */
    public function getRecipientID();

    /**
     * Get Page ID
     *
     * @return string
     */
    public function getPageID();

    /**
     * Get Timestamp
     *
     * @return int
     */
    public function getTimestamp();

    /**
     * Set State
     *
     * @param $state
     * @param null $val
     * @return $this
     */
    public function setState( $state, $val = null );

    /**
     * Get State
     *
     * @param null $key
     * @param null $def
     * @return string|array|null
     */
    public function getState( $key = null, $def = null );

    /**
     * Check has State
     *
     * @param $key
     * @return bool
     */
    public function hasState( $key );

    /**
     * Remove State
     *
     * @param $key
     * @return $this
     */
    public function removeState( $key );

    /**
     * Get Session ID
     *
     * @return string
     */
    public function getSessionID();

    /**
     * Read the State for Event
     *
     * @param bool $reload
     * @return $this
     */
    public function readState( $reload = false );

    /**
     * Reset State
     *
     * @return $this
     */
    public function resetState();

    /**
     * Save State
     *
     * @param array $append
     * @return $this
     */
    public function saveState( $append = array() );

    /**
     * Clear State
     *
     * @return $this
     */
    public function clearState();

    /**
     * To String
     *
     * @return string
     */
    public function toString();

    /**
     * Get the User Profile
     *
     * @return mixed
     */
    public function getProfile();

    /**
     * Validate the request for event
     *
     * @return bool
     */
    public function isValidRequest();

    /**
     * Create Reply Object
     *
     * @param $message
     * @param null $messageClass
     * @return mixed
     */
    public function replyObject( $message, $messageClass = null );

    /**
     * Reply
     *
     * @param $message
     * @param callable|null $callback
     * @param null $messageClass
     * @throws GeneralException
     * @return MessengerResponseInterface
     */
    public function reply( $message, callable $callback = null, $messageClass = null );

    /**
     * Reply with AI
     *
     * @param callable|null $callback
     * @return MessengerResponseInterface
     */
    public function replyWithAI( callable $callback = null );
}