<?php namespace Ayedev\Bot\Messenger\IFace;

use Ayedev\Bot\Messenger\Core\ApiCall;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

interface WebhookInterface
{
    /**
     * Validate Bot Callback
     *
     * @return bool
     */
    public function validateBotCallback();

    /**
     * Validate Hub Signature
     *
     * @return bool
     */
    public function isValidHubSignature();

    /**
     * Get States Directory
     *
     * @return string
     */
    public function getStateDir();

    /**
     * Get Cache Directory
     *
     * @return string
     */
    public function getCacheDir();

    /**
     * Create ApiCall
     *
     * @return ApiCall
     */
    public function createApiCall();

    /**
     * Get ApiCall
     *
     * @return ApiCall
     */
    public function getApiCall();


    /**
     * Validate Webhook
     *
     * @return bool
     */
    public function validateBotChallenge();

    /**
     * Verify Challenge
     *
     * @param $verify
     * @return bool
     */
    public function verifyChallenge( $verify );

    /**
     * Get Challenge Response
     *
     * @return string|null
     */
    public function challengeResponse();

    /**
     * Set Request
     *
     * @param $request
     * @return $this
     */
    public function setRequest( $request );

    /**
     * Get Request
     *
     * @return ServerRequestInterface
     */
    public function getRequest();

    /**
     * Get Events
     *
     * @return array
     */
    public function getEvents();

    /**
     * Locate for Event for Session ID
     *
     * @param string $session_id
     * @return EventInterface|null
     */
    public function eventForSession( $session_id );

    /**
     * Add Event Subscriber
     *
     * @param EventSubscriberInterface $subscriber
     * @return $this
     */
    public function addEventSubscriber( EventSubscriberInterface $subscriber );

    /**
     * Listen for Event
     *
     * @param $name
     * @param callable $callback
     * @param int $priority
     * @return $this
     */
    public function listenEvent( $name, callable $callback, $priority = 0 );

    /**
     * Dispatch Callback Events
     *
     * @return $this
     */
    public function dispatchCallbackEvents();

    /**
     * Set Secret
     *
     * @param $secret
     * @return $this
     */
    public function setSecret( $secret );

    /**
     * Get Secret
     *
     * @return string
     */
    public function getSecret();

    /**
     * Set Challenge
     *
     * @param $challenge
     * @return $this
     */
    public function setBotChallenge( $challenge );

    /**
     * Get Challenge
     *
     * @return string
     */
    public function getBotChallenge();

    /**
     * Get Headers for Request
     * 
     * @return array
     */
    public function getHeaders();
}