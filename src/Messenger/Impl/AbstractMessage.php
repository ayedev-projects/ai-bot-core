<?php namespace Ayedev\Bot\Messenger\Impl;

use Ayedev\Bot\Messenger\Traits\MessageTrait;

abstract class AbstractMessage implements \JsonSerializable, \ArrayAccess
{
    use MessageTrait;


    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Return
        return '[UNABLE_TO_READ]';
    }

    /**
     * Check has File to Upload
     *
     * @return bool
     */
    public function hasFileToUpload()
    {
        //  Return
        return false;
    }

    /**
     * Get File Stream
     *
     * @return bool
     */
    public function getFileStream()
    {
        //  Return
        return null;
    }
}