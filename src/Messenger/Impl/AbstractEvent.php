<?php namespace Ayedev\Bot\Messenger\Impl;

use Ayedev\Bot\AI\IFace\AIResponseInterface;
use Ayedev\Bot\Messenger\Event\ChatEvent;
use Ayedev\Bot\Messenger\Exception\GeneralException;
use Ayedev\Bot\Messenger\IFace\EventInterface;
use Ayedev\Bot\Messenger\Traits\KeyValuePairsTrait;
use Ramsey\Uuid\Uuid;
use Symfony\Component\EventDispatcher\Event;

abstract class AbstractEvent extends Event implements EventInterface
{
    use KeyValuePairsTrait;

    /** @var AIResponseInterface $_response */
    protected $_response;

    /** @var AbstractEntry $_entry */
    protected $_entry;

    /** @var array $_sender */
    protected $_sender;

    /** @var array $_recipient */
    protected $_recipient;

    /** @var string $_page_id */
    protected $_page_id;

    /** @var int $_time */
    protected $_time;

    /** @var bool $_prevented */
    protected $_prevented = false;


    /**
     * Destructor
     */
    public function __destruct()
    {
        //  Save State
        $this->saveState();
    }

    /**
     * @inheritdoc
     */
    public function preventDefault()
    {
        //  Prevent
        $this->_prevented = true;

        //  Return
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isDefaultPrevented()
    {
        //  Return
        return $this->_prevented;
    }

    /**
     * @inheritdoc
     */
    public function attachEntry( AbstractEntry $entry )
    {
        //  Store
        $this->_entry = $entry;

        //  Return
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function attachResponse( AIResponseInterface $response )
    {
        //  Store
        $this->_response = $response;

        //  Return
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getEntry()
    {
        //  Return
        return $this->_entry;
    }

    /**
     * @inheritdoc
     */
    public function getResponse()
    {
        //  Return
        return $this->_response;
    }

    /**
     * @inheritdoc
     */
    public function hasResponse()
    {
        //  Return
        return ( $this->_response && !is_null ( $this->_response ) );
    }

    /**
     * @inheritdoc
     */
    public function getSender()
    {
        //  Return
        return $this->_sender;
    }

    /**
     * @inheritdoc
     */
    public function getSenderID()
    {
        //  Return
        return $this->_sender['id'];
    }

    /**
     * @inheritdoc
     */
    public function getRecipient()
    {
        //  Return
        return $this->_recipient;
    }

    /**
     * @inheritdoc
     */
    public function getRecipientID()
    {
        //  Return
        return $this->_recipient['id'];
    }

    /**
     * @inheritdoc
     */
    public function getPageID()
    {
        //  Return
        return $this->_page_id;
    }

    /**
     * @inheritdoc
     */
    public function getTimestamp()
    {
        //  Return
        return $this->_time;
    }


    /**
     * @inheritdoc
     */
    public function setState( $state, $val = null )
    {
        //  Check
        if( sizeof( func_get_args() ) > 1 )
        {
            //  Return
            return $this->setValue( $state, $val );
        }

        //  Return
        return $this->assignValues( $state );
    }

    /**
     * @inheritdoc
     */
    public function getState( $key = null, $def = null )
    {
        //  Return
        return $this->getValue( $key, $def );
    }

    /**
     * @inheritdoc
     */
    public function hasState( $key )
    {
        //  Return
        return $this->hasValue( $key );
    }

    /**
     * @inheritdoc
     */
    public function removeState( $key )
    {
        //  Return
        return $this->removeValue( $key );
    }

    /**
     * Generate Session ID
     *
     * @return string
     */
    public function generateSessionID()
    {
        //  Return
        return Uuid::uuid4()->toString();
    }

    /**
     * Generate Session ID
     *
     * @return string
     */
    public function generateFilename()
    {
        //  Return
        return md5( $this->getPageID() . ':' . $this->getSenderID() );
    }

    /**
     * @inheritdoc
     */
    public function getSessionID()
    {
        //  Return
        return $this->getValue( 'session_id' );
    }

    /**
     * Get State File Path
     *
     * @return string
     */
    public function getStateFile()
    {
        //  Return
        return mManager()->getDataPath( $this->generateFilename() . '.json', mManager()->getWebhook()->getStateDir() );
    }

    /**
     * @inheritdoc
     */
    public function readState( $reload = false )
    {
        //  Check
        if( !$this->hasValues() || $reload )
        {
            //  Check
            if( file_exists( ( $state_file = $this->getStateFile() ) ) )
            {
                //  Read State Data
                $this->setState( json_decode( file_get_contents( $this->getStateFile() ), true ) );
            }
            else
            {
                //  Reset State
                $this->resetState();
            }
        }

        //  Return
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function resetState()
    {
        //  Reset Data
        $this->assignValues( array(
            'time' => $this->getTimestamp(),
            'page_id' => $this->getPageID(),
            'sender_id' => $this->getSenderID(),
            'recipient_id' => $this->getRecipientID(),
            'session_id' => $this->generateSessionID()
        ) );

        //  Return
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function saveState( $append = array() )
    {
        //  New State
        $this->mergeValues( $append );

        //  Get State File Path
        $stateFilePath = $this->getStateFile();

        //  Check
        if( !file_exists( $stateDir = dirname( $stateFilePath ) ) )
        {
            //  Make Dir
            mkdir( $stateDir, 0775, true );
        }

        //  Save State
        file_put_contents( $stateFilePath, json_encode( $this->toArray() ) );

        //  Return
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function clearState()
    {
        //  Clear State
        $this->clearValues();

        //  Check
        if( file_exists( ( $state_file = $this->getStateFile() ) ) )
        {
            //  Delete State
            unlink( $state_file );
        }

        //  Return
        return $this;
    }


    /**
     * Magic Call
     *
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws \Exception
     */
    public function __call( $name, $arguments )
    {
        //  Check
        if( method_exists( $this->getEntry(), $name ) )
        {
            //  Return
            return call_user_func_array( array( $this->getEntry(), $name ), $arguments );
        }

        //  Return
        return $this->respondWithKeyPairs( $name, $arguments );
    }


    /**
     * @inheritdoc
     */
    public function toString()
    {
        //  Output
        $output = '[UNABLE_TO_READ]';

        //  Switch
        switch( $this->getType() )
        {
            //  Message
            case static::MESSAGE_RECEIVED:

                //  Get Text
                $output = $this->getText();

                //  Check
                if( $this->isQuickReply() )
                {
                    //  Get Quick Reply Payload
                    //$output = $this->getQuickReplyPayload();
                }
                break;

            //  Postback
            case static::MESSAGE_POSTBACK:

                //  Get Payload
                $output = $this->getPostbackPayload();
                break;

            //  Static Only
            case static::MESSAGE_ECHO:
            case static::MESSAGE_DELIVERY:
            case static::LOCATION_RECEIVED:
            case static::MESSAGE_READ:

                //  Get Text
                $output = '[' . strtoupper( $this->getType() ) . ']';
                break;
        }

        //  Return
        return $output;
    }

    /**
     * @inheritdoc
     */
    public function isValidRequest()
    {
        //  Get Event for Session
        $event = mManager()->getWebhook()->eventForSession( $this->getSessionID() );

        //  Return
        return ( $event && !is_null( $event ) );
    }

    /**
     * @inheritdoc
     */
    public function reply( $message, callable $callback = null, $messageClass = null )
    {
        //  Check is real request
        if( $this->isValidRequest() )
        {
            //  Make Reply
            $reply = ( $message instanceof AbstractMessage ? $message : $this->replyObject( $message, $messageClass ) );

            //  Run
            $callback && is_callable( $callback ) && call_user_func_array( \Closure::bind( $callback, $this ), array( $reply ) );

            //  Get Response
            $response = mManager()->getMessenger()->sendMessage( $this->getSenderID(), $reply );

            //  Trigger ChatEvent
            ChatEvent::dispatchResponseEvent( $this, $reply );

            //  Return
            return $response;
        }

        //  Throw Exception
        throw ( new GeneralException( "Unable to reply on invalid request" ) )->setObject( $message );
    }

    /**
     * @inheritdoc
     */
    public function replyWithAI( callable $callback = null )
    {
        //  Return
        return mManager()->getAI()->sendResponse( $this );
    }
}