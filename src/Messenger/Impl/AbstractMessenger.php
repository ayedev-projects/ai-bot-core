<?php namespace Ayedev\Bot\Messenger\Impl;

use Ayedev\Bot\Messenger\Core\MessengerResponse;
use Ayedev\Bot\Messenger\IFace\MessengerInterface;

abstract class AbstractMessenger extends AbstractSkeleton implements MessengerInterface
{
    /**
     * Send Message
     *
     * @return mixed
     */
    public static function send()
    {
        //  Return
        return self::call( 'sendMessage', func_get_args() );
    }
}