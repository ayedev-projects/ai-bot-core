<?php namespace Ayedev\Bot\Messenger\Impl;

use Ayedev\Bot\Messenger\IFace\EventInterface;

abstract class AbstractList
{
    /** @var array $_list */
    protected $_list = array();


    /**
     * Add Entry
     *
     * @param EventInterface $event
     * @return $this
     */
    public function add( EventInterface $event )
    {
        //  Store
        $this->_list[] = $event;

        //  Return
        return $this;
    }

    /**
     * Get All List
     *
     * @return array
     */
    public function all()
    {
        //  Return
        return $this->_list;
    }
}