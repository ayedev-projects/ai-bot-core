<?php namespace Ayedev\Bot\Messenger\Impl;

abstract class AbstractSkeleton
{
    /** @var array $_instance */
    private static $_instances = array();


    /**
     * Set Instance
     *
     * @param $instance
     */
    public static function setInstance( $instance )
    {
        //  Class
        $class = get_called_class();

        //  Store
        self::$_instances[$class] = $instance;
    }

    /**
     * Get the Instance
     *
     * @param array $args
     * @return mixed
     */
    public static function instance( ...$args )
    {
        //  Class
        $class = get_called_class();

        //  Check
        if( !isset( self::$_instances[$class] ) )
        {
            //  Create Instance
            self::$_instances[$class] = new static( ...$args );
        }

        //  Return
        return self::$_instances[$class];
    }

    /**
     * Call Methods Statically
     *
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public static function __callStatic( $name, $arguments )
    {
        //  Return
        return self::call( 'static_' . $name, $arguments );
    }

    /**
     * Call Method
     *
     * @param $name
     * @param array $arguments
     * @return mixed
     */
    public static function call( $name, $arguments = array() )
    {
        //  Return
        return call_user_func_array( array( self::instance(), $name ), ( is_array( $arguments ) && isset( $arguments[0] ) ? $arguments : (array)$arguments ) );
    }
}