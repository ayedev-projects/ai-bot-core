<?php namespace Ayedev\Bot\Messenger\Impl;

use Ayedev\Bot\Messenger\Traits\KeyValuePairsTrait;

abstract class AbstractEntry
{
    use KeyValuePairsTrait;

    /** @var string $_type */
    protected $_type = 'raw';

    /** @var array $_raw */
    protected $_raw = array();


    /**
     * Entry constructor.
     *
     * @param array|null $data
     * @param string|null $type
     * @param array|null $raw
     */
    public function __construct( $data = array(), $type = null, $raw = null )
    {
        //  Store
        $this->assignValues( $data ?: array() );

        //  Check
        if( $type ) $this->_type = $type;

        //  Check
        if( $raw ) $this->_raw = $raw;
    }

    /**
     * Get Type
     *
     * @return null|string
     */
    public function getType()
    {
        //  Return
        return $this->_type;
    }

    /**
     * Get Raw Data
     *
     * @return null|array
     */
    public function getRaw()
    {
        //  Return
        return $this->_raw;
    }

    /**
     * Get Name
     *
     * @return null|string
     */
    public function getName()
    {
        //  Return
        return ( isset( $this->name ) ? $this->name : $this->getType() );
    }

    /**
     * Check Type
     *
     * @param $type
     * @return bool
     */
    public function is( $type )
    {
        //  Return
        return ( $type === $this->getType() );
    }
}