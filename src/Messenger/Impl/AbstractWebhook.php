<?php namespace Ayedev\Bot\Messenger\Impl;

use GuzzleHttp\Psr7\ServerRequest;
use Ayedev\Bot\Messenger\Core\ApiCall;
use Ayedev\Bot\Messenger\Core\XHubSignature;
use Ayedev\Bot\Messenger\Event\ChatEvent;
use Ayedev\Bot\Messenger\IFace\EventInterface;
use Ayedev\Bot\Messenger\IFace\WebhookInterface;
use Psr\Http\Message\ServerRequestInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

abstract class AbstractWebhook extends AbstractSkeleton implements WebhookInterface
{
    /** @var string $_secret */
    protected $_secret;

    /** @var string $_challenge */
    protected $_challenge;

    /** @var ServerRequestInterface $_request */
    protected $_request;

    /** @var array $_decodedBody */
    protected $_decodedBody;

    /** @var array $_events */
    protected $_events = array();

    /** @var string $_body */
    protected $_body;

    /** @var ApiCall $_apiCall */
    protected $_apiCall;


    /**
     * Webhook constructor.
     *
     * @param $secret
     * @param $challenge
     * @param ApiCall|null $apiCall
     */
    public function __construct( $secret = null, $challenge = null, ApiCall $apiCall = null )
    {
        //  Store Secret
        $this->setSecret( $secret );

        //  Store Bot Challenge
        $this->setBotChallenge( $challenge );

        //  Store Request
        $this->setRequest( ServerRequest::fromGlobals() );

        //  Store
        $this->_apiCall = $apiCall ?: $this->createApiCall();
    }

    /**
     * @inheritdoc
     */
    public function getApiCall()
    {
        //  Return
        return $this->_apiCall;
    }

    /**
     * @inheritdoc
     */
    public function getHeaders()
    {
        //  Return
        return array(
            'X-AYEDEV-BOT-REQUEST' => '1',
            'X-AYEDEV-BOT-VERSION' => AYEDEV_BOT_LIB_VERSION,
            'X-AYEDEV-BOT-SOURCE' => 'messenger',
            'X-AYEDEV-BOT-PLATFORM' => 'php',
            'Accept' => 'application/json',
            'Content-Type' => 'application/json; charset=utf-8'
        );
    }


    /**
     * Validate Hub Signature
     *
     * @return bool
     */
    public function isValidHubSignature()
    {
        //  Get Signature
        $headers = $this->getRequest()->getHeader( 'X-Hub-Signature' );

        //  Check
        if( !$headers && isset( $_SERVER['HTTP_X_HUB_SIGNATURE'] ) )    $headers = array( $_SERVER['HTTP_X_HUB_SIGNATURE'] );

        //  Check Headers
        if( empty( $headers ) ) return false;

        //  Get Signature
        $signature = XHubSignature::parseHeader( $headers[0] );

        //  Return
        return XHubSignature::validate( $this->getBody(), $this->getSecret(), $signature );
    }

    /**
     * Validate Webhook
     *
     * @return mixed
     */
    public static function validateCallback()
    {
        //  Return
        return self::call( 'validateBotCallback' );
    }

    /**
     * Get States Directory
     *
     * @return string
     */
    public function getStateDir()
    {
        //  Return
        return '.states';
    }

    /**
     * Get Cache Directory
     *
     * @return string
     */
    public function getCacheDir()
    {
        //  Return
        return '.cache';
    }


    /**
     * Validate Webhook
     *
     * @return bool
     */
    public function validateBotChallenge()
    {
        //  Check Method
        if( $this->getRequest()->getMethod() !== 'GET' )
        {
            //  Return
            return false;
        }

        //  Get Query Params
        $params = $this->getRequest()->getQueryParams();

        //  Check for Token
        if( !isset( $params['hub_verify_token'] ) )
        {
            //  Return
            return false;
        }

        //  Return
        return $this->verifyChallenge( $params['hub_verify_token'] );
    }

    /**
     * Verify Challenge
     *
     * @param $verify
     * @return bool
     */
    public function verifyChallenge( $verify )
    {
        //  Return
        return ( $this->getBotChallenge() === $verify );
    }

    /**
     * Get Challenge Response
     *
     * @return string|null
     */
    public function challengeResponse()
    {
        //  Get Query Params
        $params = $this->getRequest()->getQueryParams();

        //  Return
        return ( isset( $params['hub_challenge'] ) ? $params['hub_challenge'] : null );
    }

    /**
     * Validate Webhook
     *
     * @return mixed
     */
    public static function validateBot()
    {
        //  Return
        return self::call( 'validateBotChallenge' );
    }

    /**
     * Get Challenge Response
     *
     * @return mixed
     */
    public static function challenge()
    {
        //  Return
        return self::call( 'challengeResponse' );
    }

    /**
     * Set Request
     *
     * @param $request
     * @return $this
     */
    public function setRequest( $request )
    {
        //  Store
        $this->_request = $request;

        //  Return
        return $this;
    }

    /**
     * Get Request
     *
     * @return ServerRequestInterface
     */
    public function getRequest()
    {
        //  Return
        return $this->_request;
    }

    /**
     * Get Events
     *
     * @return array
     */
    public function getEvents()
    {
        //  Return
        return $this->generateEvents();
    }

    /**
     * Generate Events
     *
     * @return array
     */
    abstract protected function generateEvents();

    /**
     * Get Decoded Body
     * @return array|mixed
     */
    public function getDecodedBody()
    {
        //  Check
        if( $this->_decodedBody )   return $this->_decodedBody;

        //  Decode
        $decoded = @json_decode( $this->getBody() , true );

        //  Return
        return $this->_decodedBody = null === $decoded ? [] : $decoded;
    }

    /**
     * Get Body
     *
     * @return string
     */
    private function getBody()
    {
        //  Check
        if( $this->_body )   return $this->_body;

        //  Capture
        $this->_body = (string) $this->getRequest()->getBody();

        //  Return
        return $this->_body;
    }

    /**
     * @inheritdoc
     */
    public function eventForSession( $session_id )
    {
        //  Event
        $event = null;

        /**
         * Loop Each
         *
         * @var AbstractList $rawEvents
         */
        foreach( $this->getEvents() as $rawEvents )
        {
            /**
             * Loop Each
             *
             * @var AbstractEvent $rawEvent
             */
            foreach( $rawEvents->all() as $rawEvent )
            {
                //  Check
                if( $rawEvent->getSessionID() === $session_id )
                {
                    //  Break
                    $event = $rawEvent;
                    break;
                }
            }

            //  Check
            if( $event )
            {
                //  Break
                break;
            }
        }

        //  Return
        return $event;
    }

    /**
     * Add Event Subscriber
     *
     * @param EventSubscriberInterface $subscriber
     * @return $this
     */
    public function addEventSubscriber( EventSubscriberInterface $subscriber )
    {
        //  Add
        mManager()->addSubscriber( $subscriber );

        //  Return
        return $this;
    }

    /**
     * Listen for Event
     *
     * @param $name
     * @param callable $callback
     * @param int $priority
     * @return $this
     */
    public function listenEvent( $name, callable $callback, $priority = 0 )
    {
        //  Add Event Callback
        mManager()->addListener( $name, $callback, $priority );

        //  Return
        return $this;
    }

    /**
     * Get Callback Events
     *
     * @return array
     */
    public function getAllCallbackEvents()
    {
        //  Events
        $events = [];

        //  Loop Each
        foreach( $this->getEvents() as $eventList )
        {
            //  Merge
            $events = array_merge( $events, $eventList->all() );
        }

        //  Return
        return $events;
    }

    /**
     * Dispatch Callback Events
     *
     * @return $this
     */
    public function dispatchCallbackEvents()
    {
        /**
         * Loop Each Callback Events
         *
         * @var AbstractEvent $event
         */
        foreach( $this->getAllCallbackEvents() as $event )
        {
            //  Trigger ChatEvent
            ChatEvent::dispatchRequestEvent( $event );

            //  Check
            if( $event->is( EventInterface::MESSAGE_RECEIVED ) && $event->isQuickReply() )
            {
                //  Dispatch
                mManager()->dispatch( 'quick_reply.' . $event->getQuickReplyPayload(), $event );
            }
            else if( $event->is( EventInterface::MESSAGE_POSTBACK ) )
            {
                //  Dispatch
                mManager()->dispatch( 'postback.' . $event->getPostbackPayload(), $event );
            }

            //  Dispatch
            mManager()->dispatch( $event->getName(), $event );

            //  Dispatch
            mManager()->dispatch( '__global__', $event );
        }

        //  Return
        return $this;
    }

    /**
     * Dispatch Events Helper
     *
     * @return mixed
     */
    public static function dispatch()
    {
        //  Return
        return self::call( 'dispatchCallbackEvents' );
    }


    /**
     * Set Secret
     *
     * @param $secret
     * @return $this
     */
    public function setSecret( $secret )
    {
        //  Store
        $this->_secret = $secret;

        //  Return
        return $this;
    }

    /**
     * Get Secret
     *
     * @return string
     */
    public function getSecret()
    {
        //  Return
        return $this->_secret;
    }

    /**
     * Set Challenge
     *
     * @param $challenge
     * @return $this
     */
    public function setBotChallenge( $challenge )
    {
        //  Store
        $this->_challenge = $challenge;

        //  Return
        return $this;
    }

    /**
     * Get Challenge
     *
     * @return string
     */
    public function getBotChallenge()
    {
        //  Return
        return $this->_challenge;
    }
}