<?php namespace Ayedev\Bot\Messenger\Exception;

class UnknownMessageException extends MessengerException {}