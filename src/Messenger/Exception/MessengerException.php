<?php namespace Ayedev\Bot\Messenger\Exception;

class MessengerException extends ApiException
{
    /** @var string $_session_id */
    private $_session_id;

    /** @var mixed $_object */
    private $_object;


    /**
     * Set Session ID
     *
     * @param $session_id
     * @return $this
     */
    public function setSessionID( $session_id )
    {
        //  Store
        $this->_session_id = $session_id;

        //  Return
        return $this;
    }

    /**
     * Get Session ID
     *
     * @return string
     */
    public function getSessionID()
    {
        //  Return
        return $this->_session_id;
    }

    /**
     * Set Object
     *
     * @param $object
     * @return $this
     */
    public function setObject( $object )
    {
        //  Store
        $this->_object = $object;

        //  Return
        return $this;
    }

    /**
     * Get Object
     *
     * @return mixed
     */
    public function getObject()
    {
        //  Return
        return $this->_object;
    }
}