<?php namespace Ayedev\Bot\Messenger\Exception;

use \Exception;

class ApiException extends Exception
{
    /** @var string $_head_err */
    private $_head_err;

    /** @var string|array $_response */
    private $_response;

    /** @var mixed $_main_exception */
    private $_main_exception;


    /**
     * Set Head Error
     *
     * @param $error
     * @return $this
     */
    public function setHeadError( $error )
    {
        //  Store
        $this->_head_err = $error;

        //  Return
        return $this;
    }

    /**
     * Get Head Error
     *
     * @return string
     */
    public function getHeadError()
    {
        //  Return
        return $this->_head_err;
    }

    /**
     * Set Main Exception
     *
     * @param $exception
     * @return $this
     */
    public function setMainException( $exception )
    {
        //  Store
        $this->_main_exception = $exception;

        //  Return
        return $this;
    }

    /**
     * Get Main Exception
     *
     * @return mixed
     */
    public function getMainException()
    {
        //  Return
        return $this->_main_exception;
    }

    /**
     * Has Head Error
     *
     * @return bool
     */
    public function hasHeadError()
    {
        //  Return
        return ( $this->_head_err && strlen( $this->_head_err ) > 0 );
    }

    /**
     * Set Response
     *
     * @param array|mixed $response
     * @return $this
     */
    public function setResponse( $response )
    {
        //  Store
        $this->_response = $response;

        //  Return
        return $this;
    }

    /**
     * Get Response
     *
     * @return array|mixed
     */
    public function getResponse()
    {
        //  Return
        return $this->_response;
    }

    /**
     * Has Response
     *
     * @return bool
     */
    public function hasResponse()
    {
        //  Return
        return ( $this->_response && sizeof( $this->_response ) > 0 );
    }
}