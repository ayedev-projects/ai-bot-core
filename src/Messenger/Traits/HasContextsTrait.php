<?php namespace Ayedev\Bot\Messenger\Traits;

trait HasContextsTrait
{
    /**
     * Set Contexts
     *
     * @param array $contexts
     * @return $this
     */
    public function setContexts( $contexts )
    {
        //  Store
        $this->setState( 'contexts', $contexts );

        //  Return
        return $this;
    }

    /**
     * Add Context
     *
     * @param $key
     * @param $data
     * @return $this
     */
    public function addContext( $key, $data )
    {
        //  Get Current
        $contexts = $this->getContexts();

        //  Add
        $contexts[$key] = $data;

        //  Return
        return $this->setContexts( $contexts );
    }

    /**
     * Remove Context(s)
     *
     * @param $keys
     * @return $this
     */
    public function removeContext( $keys )
    {
        //  Get Current
        $contexts = $this->getContexts();

        //  Loop Each
        foreach( (array)$keys as $key )
        {
            //  Check
            if( isset( $contexts[$key] ) )
            {
                //  Unset
                unset( $contexts[$key] );
            }
        }

        //  Return
        return $this->setContexts( $contexts );
    }

    /**
     * Keep only Contexts
     *
     * @param $keys
     * @return $this
     */
    public function onlyContexts( $keys )
    {
        //  Get Current
        $contexts = $this->getContexts();

        //  Loop Each
        foreach( (array)$keys as $key )
        {
            //  Check
            if( !isset( $contexts[$key] ) )
            {
                //  Unset
                unset( $contexts[$key] );
            }
        }

        //  Return
        return $this->setContexts( $contexts );
    }

    /**
     * Get Contexts
     *
     * @return array
     */
    public function getContexts()
    {
        //  Return
        return $this->getState( 'contexts', array() );
    }

    /**
     * Get Context Keys
     *
     * @return array
     */
    public function getContextKeys()
    {
        //  Return
        return array_keys( $this->getContexts() );
    }

    /**
     * Sync Contexts
     *
     * @param string|array $keys
     * @return $this
     */
    public function syncContexts( $keys )
    {
        //  New Contexts
        $contexts = array();

        //  Loop Each
        foreach( $this->getContexts() as $key => $data )
        {
            //  Check
            if( in_array( $key, (array)$keys ) )
            {
                //  Store
                $contexts[$key] = $data;
            }
        }

        //  Return
        return $this;
    }

    /**
     * Clear Contexts
     *
     * @return $this
     */
    public function clearContexts()
    {
        //  Clear
        $this->removeState( 'contexts' );

        //  Return
        return $this;
    }
}