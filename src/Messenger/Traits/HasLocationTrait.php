<?php namespace Ayedev\Bot\Messenger\Traits;

trait HasLocationTrait
{
    /**
     * Save Location
     *
     * @param $location
     * @return $this
     */
    public function saveLocation( $location )
    {
        //  Save
        $this->setState( 'location', $location );

        //  Return
        return $this;
    }

    /**
     * Get Location
     *
     * @return array
     */
    public function getLocation()
    {
        //  Return
        return $this->getState( 'location' );
    }

    /**
     * Get Location Latitude
     *
     * @return null
     */
    public function getLocationLat()
    {
        //  Check
        if( !$this->hasLocation() )  return null;

        //  Get Payload
        $payload = $this->getLocation();

        //  Return
        return $payload['payload']['coordinates']['lat'];
    }

    /**
     * Get Location Longitude
     *
     * @return null
     */
    public function getLocationLng()
    {
        //  Check
        if( !$this->hasLocation() )  return null;

        //  Get Payload
        $payload = $this->getLocation();

        //  Return
        return $payload['payload']['coordinates']['long'];
    }

    /**
     * Check has Location
     *
     * @return bool
     */
    public function hasLocation()
    {
        //  Return
        return $this->hasState( 'location' );
    }

    /**
     * Clear Location
     *
     * @return $this
     */
    public function clearLocation()
    {
        //  Store History
        $this->setState( 'last_location', $this->getLocation() );

        //  Clear Location
        $this->removeState( 'location' );

        //  Return
        return $this;
    }
}