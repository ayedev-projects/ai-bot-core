<?php namespace Ayedev\Bot\Messenger\Traits;

trait KeyValuePairsTrait
{
    /** @var array $_ignore */
    private $_ignore = array();

    /** @var array $_convert */
    private $_convert = array();

    /** @var array $_data */
    private $_data = array();


    /**
     * Add Ignore
     *
     * @param $ignore
     * @return $this
     */
    public function addIgnore( $ignore )
    {
        //  Add Ignore
        $this->_ignore[] = $ignore;

        //  Return
        return $this;
    }

    /**
     * Add Key Convert
     *
     * @param $key
     * @param $realKey
     */
    public function addKeyConvert( $key, $realKey )
    {
        //  Store
        $this->_convert[$key] = $realKey;
    }


    /**
     * Magic Method: Setter
     *
     * @param $key
     * @param $val
     */
    public function __set( $key, $val )
    {
        //  Set Value
        $this->setValue( $key, $val );
    }

    /**
     * Magic Method: Getter
     *
     * @param $key
     * @return null
     */
    public function __get( $key )
    {
        //  Return
        return $this->getValue( $key );
    }

    /**
     * Magic Method: Isset
     *
     * @param $key
     * @return bool
     */
    public function __isset( $key )
    {
        //  Return
        return $this->hasValue( $key );
    }

    /**
     * Magic Method: Unset
     *
     * @param $key
     */
    public function __unset( $key )
    {
        //  Remove
        $this->removeValue( $key );
    }

    /**
     * @inheritdoc
     */
    public function offsetExists( $key )
    {
        //  Return
        return $this->hasValue( $key );
    }

    /**
     * @inheritdoc
     */
    public function offsetGet( $key )
    {
        //  Return
        return $this->getValue( $key );
    }

    /**
     * @inheritdoc
     */
    public function offsetSet( $key, $val )
    {
        //  Set Value
        $this->setValue( $key, $val );
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset( $key )
    {
        //  Remove Value
        $this->removeValue( $key );
    }


    /**
     * Set Value
     *
     * @param $key
     * @param $val
     * @return $this
     */
    public function setValue( $key, $val )
    {
        //  Store
        $this->_data[$key] = $val;

        //  Return
        return $this;
    }

    /**
     * Get Value
     *
     * @param $key
     * @param null $def
     * @return null
     */
    public function getValue( $key, $def = null )
    {
        //  Return
        return ( $key ? ( $this->hasValue( $key ) ? $this->_data[$key] : $def ) : $this->getValues() );
    }

    /**
     * Check for Value
     *
     * @param $key
     * @return bool
     */
    public function hasValue( $key )
    {
        //  Return
        return isset( $this->_data[$key] );
    }

    /**
     * Remove Value
     *
     * @param $key
     * @return $this
     */
    public function removeValue( $key )
    {
        //  Check
        if( $this->hasValue( $key ) )
        {
            //  Remove
            unset( $this->_data[$key] );
        }

        //  Return
        return $this;
    }

    /**
     * Get Values
     *
     * @return array
     */
    public function getValues()
    {
        //  Return
        return $this->_data;
    }

    /**
     * Clear Values
     *
     * @return $this
     */
    public function clearValues()
    {
        //  Return
        return $this->assignValues( array() );
    }

    /**
     * Assign Values
     *
     * @param $data
     * @return $this
     */
    public function assignValues( $data )
    {
        //  Store
        $this->_data = $data;

        //  Return
        return $this;
    }

    /**
     * Merge Values
     *
     * @param $data
     * @return $this
     */
    public function mergeValues( $data )
    {
        //  Store
        $this->_data = array_merge( $this->_data, $data );

        //  Return
        return $this;
    }

    /**
     * Check has values
     *
     * @return bool
     */
    public function hasValues()
    {
        //  Return
        return ( sizeof( $this->_data ) > 0 );
    }


    /**
     * JSON Serialize
     *
     * @return array|null
     */
    public function jsonSerialize()
    {
        //  Get Data
        $data = $this->toArray();

        //  Return
        return ( $data ?: null );
    }

    /**
     * Convert to Valid Array Data
     *
     * @return array
     */
    public function toArray()
    {
        //  Result
        $result = array();

        //  Loop Each
        foreach( $this->_data as $key => $val )
        {
            //  Check
            if( is_null( $val ) || ( is_array( $val ) && sizeof( $val ) < 1 ) || ( is_string( $val ) && empty( $val ) ) )   continue;

            //  Check for Convert
            if( isset( $this->_convert[$key] ) )
            {
                //  Convert
                $key = ( is_callable( $this->_convert[$key] ) ? call_user_func_array( $this->_convert[$key], array( $key, $this ) ) : $this->_convert[$key] );
            }

            //  Check
            if( in_array( $key, $this->_ignore ) )  continue;

            //  Store
            $result[$key] = $val;
        }

        //  Return
        return $result;
    }


    /**
     * Magic Methods
     *
     * @param $name
     * @param $arguments
     * @return $this|null
     * @throws \Exception
     */
    public function __call( $name, $arguments )
    {
        //  Return
        return $this->respondWithKeyPairs( $name, $arguments );
    }

    /**
     * Response with Key/Pairs
     *
     * @param $name
     * @param $arguments
     * @return $this|null
     * @throws \Exception
     */
    protected function respondWithKeyPairs( $name, $arguments )
    {
        //  Check
        if( isset( $this->_fillable ) && ( is_array( $this->_fillable ) || is_bool( $this->_fillable ) ) )
        {
            //  Check for Setter
            if( substr( $name, 0, 3 ) == 'set' && sizeof( $arguments ) > 0 )
            {
                //  Field Name
                $key = $this->_to_snake( substr( $name, 3 ) );

                //  Check
                if( $this->_fillable === true || in_array( $key, $this->_fillable ) )
                {
                    //  Store
                    return $this->setValue( $key, $arguments[0] );
                }
            }
            //  Check for Getter
            else if( substr( $name, 0, 3 ) == 'get' )
            {
                //  Field Name
                $key = $this->_to_snake( substr( $name, 3 ) );

                //  Check
                if( $this->_fillable === true || in_array( $key, $this->_fillable ) )
                {
                    //  Retrieve
                    return $this->getValue( $key, ( sizeof( $arguments ) > 0 ? $arguments[0] : null ) );
                }
            }
        }

        //  Throw Exception
        throw new \Exception( "Method {$name} not found in class " . get_called_class() );
    }

    /**
     * Convert to Snake Case
     *
     * @param $str
     * @param string $sep
     * @return string
     */
    protected function _to_snake( $str, $sep = '_' )
    {
        //  Matches
        $matches = null;
        preg_match_all( '!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $str, $matches );

        //  Get
        $ret = ( $matches ? $matches[0] : null );

        //  Check
        if( $ret )
        {
            //  Loop Each
            foreach( $ret as &$match )
            {
                //  Fix
                $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
            }
        }

        //  Return
        return trim( implode( $sep, $ret ), $sep );
    }
}