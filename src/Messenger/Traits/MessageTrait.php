<?php namespace Ayedev\Bot\Messenger\Traits;

trait MessageTrait
{
    use KeyValuePairsTrait, PlainMessageTrait;
}