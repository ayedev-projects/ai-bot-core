<?php

//  Load Libraries
use Ayedev\Bot;
use SSD\DotEnv\DotEnv;

//  Define
define( 'AYEDEV_BOT_LIB_VERSION', '1.0' );


/**
 * Get the Default Platform
 *
 * @param null $platform
 * @return Manager
 */
function defaultPlatformManager( $platform = null )
{
    //  Global
    global $def_platform_manager;

    //  Check
    if( $platform )
    {
        //  Assign
        $def_platform_manager = $platform;
    }

    //  Check
    if( !$def_platform_manager )    $def_platform_manager = 'facebook';

    //  Return
    return $def_platform_manager;
}

/**
 * Get the Manager
 *
 * @param array ...$args
 * @return Manager
 */
function mManager( ...$args )
{
    //  Global
    global $mManagers;

    //  Check
    if( !$mManagers )   $mManagers = array();

    //  Platform
    $platform = ( sizeof( func_get_args() ) > 0 ? func_get_arg(0 ) : null );

    //  Fix
    if( !$platform )    $platform = defaultPlatformManager();

    //  Check
    if( !isset( $mManagers[$platform] ) )   $mManagers[$platform] = new Bot\Messenger\Core\Manager( ...$args );

    //  Return
    return $mManagers[$platform];
}

/**
 * Get All Managers
 *
 * @return array
 */
function mManagers()
{
    //  Global
    global $mManagers;

    //  Check
    if( !$mManagers )   $mManagers = array();

    //  Return
    return $mManagers;
}


//  Check
if( !function_exists( 'dd' ) ):

    //  Dumper
    function dd( ...$args )
    {
        //  Loop Each
        foreach( $args as $arg )
        {
            //  Print Start
            if( !function_exists( 'xdebug_enable' ) )   echo '<pre>';

            //  Dump
            var_dump( $arg );

            //  Print Start
            if( !function_exists( 'xdebug_enable' ) )   echo '</pre>';
        }

        //  Exit Script
        exit;
    }
endif;


/**
 * Get Config
 *
 * @param $key
 * @param null $def
 * @return bool|mixed|null
 */
function bot_config( $key, $def = null )
{
    //  Return
    return ( DotEnv::has( $key ) ? DotEnv::get( $key, $def ) : ( isset( $_ENV[$key] ) ? $_ENV[$key] : $def ) );
}

/**
 * Get Config Boolean
 *
 * @param $key
 * @param bool $def
 * @return bool
 */
function bot_config_bool( $key, $def = false )
{
    //  Return
    return ( bot_config( $key, '0' ) == '1' ? true : $def );
}

/**
 * Get Config Array
 *
 * @param $key
 * @param array $def
 * @param string $sep
 * @return array|null
 */
function bot_config_arr( $key, $def = array(), $sep = ',' )
{
    //  Get Existing Value
    $val = bot_config( $key, $def );

    //  Check
    if( !is_array( $val ) )
    {
        //  Create Array
        $val = explode( $sep, $val );
    }

    //  Return
    return $val;
}


//  Create/Retrieve Instance
function class_object( $class, ...$args )
{
    //  Global
    global $_reg_instances;

    //  Check
    if( !$_reg_instances )  $_reg_instances = array();

    //  Check
    if( !isset( $_reg_instances[$class] ) )
    {
        //  Store Instance
        $_reg_instances[$class] = new $class( ...$args );
    }

    //  Return
    return $_reg_instances[$class];
}

//  Register Class Instance
function register_class_instance( $class, $ins )
{
    //  Global
    global $_reg_instances;

    //  Check
    if( !$_reg_instances )  $_reg_instances = array();

    //  Store
    $_reg_instances[$class] = $ins;

    //  Return
    return $_reg_instances[$class];
}

//  Get Instance
function class_instance( $class )
{
    //  Global
    global $_reg_instances;

    //  Return
    return ( $_reg_instances && isset( $_reg_instances[$class] ) ? $_reg_instances[$class] : null );
}


/**
 * Get Error Handler
 *
 * @return Bot\Tool\ErrorHandler
 */
function bot_error_handler()
{
    //  Return
    return class_instance( 'error_handler' );
}

/**
 * Get Logger
 *
 * @return Bot\Tool\Logger
 */
function bot_logger()
{
    //  Return
    return class_instance( 'logger' );
}


//  Setup AI Bot Workspace
function setup_ai_bot_workspace( $config_file, $log_file )
{
    //  Store Instance for Logger
    register_class_instance( 'logger', new Bot\Tool\Logger( $log_file ) );

    //  Store Instance for Error Handler
    register_class_instance( 'error_handler', new Bot\Tool\ErrorHandler );

    //  Store Instance for Config Loader
    register_class_instance( 'config', new DotEnv( $config_file ) )->load();
}