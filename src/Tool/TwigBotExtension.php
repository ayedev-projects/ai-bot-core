<?php namespace Ayedev\Bot\Tool;


class TwigBotExtension extends \Twig_Extension
{
    /**
     * Define Functions
     *
     * @return array
     */
    public function getFunctions()
    {
        //  Return
        return array(
            new \Twig_SimpleFunction( 'config', 'config', array( 'is_safe' => array( 'html' ) ) ),
        );
    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'ai-bot-sdk';
    }
}