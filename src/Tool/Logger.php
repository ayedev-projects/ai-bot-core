<?php namespace Ayedev\Bot\Tool;

class Logger
{
    /** @var string $_path */
    private $_path;

    /** @var resource $_fn */
    private $_fn;

    /** @var string $_last_msg */
    private $_last_msg;

    /** @var string $_last_msg_type */
    private $_last_msg_type;


    /**
     * Logger constructor.
     *
     * @param $log_file
     * @param bool $clear
     */
    public function __construct( $log_file, $clear = false )
    {
        //  Store File Name
        $this->_path = $log_file;

        //  Check
        if( $clear )    $this->clear();

        //  Open
        $this->open();
    }

    /**
     * Open Log File
     */
    public function open()
    {
        //  Check
        if( !$this->_fn )
        {
            //  Check
            if( ( $dirname = dirname( $this->_path ) ) && !file_exists( $dirname ) )    mkdir( $dirname, 0775, true );

            //  Open File
            $this->_fn = fopen( $this->_path, 'a+' );
        }
    }

    /**
     * Close Log File
     */
    public function close()
    {
        //  Close the File
        $this->_fn && fclose( $this->_fn );

        //  Clear Resource
        $this->_fn = null;

        //  Return
        return $this;
    }

    /**
     * Clear Log File
     *
     * @return $this
     */
    public function clear()
    {
        //  Close
        $this->close();

        //  Write Contents
        file_put_contents( $this->_path, "# Logger file created @ [" . date( 'Y-m-d H:i:s T Z' ) . "]\n\n" );

        //  Open
        $this->open();
    }

    /**
     * Logger destructor
     */
    public function __destruct()
    {
        //  Close
        $this->close();
    }


    /**
     * Success Message
     *
     * @param $msg
     * @param null $title
     * @return Logger
     */
    public function success( $msg, $title = null )
    {
        //  Return
        return $this->write( $msg, 'S', $title );
    }

    /**
     * Error Message
     *
     * @param $msg
     * @param null $title
     * @return Logger
     */
    public function error( $msg, $title = null )
    {
        //  Return
        return $this->write( $msg, 'E', $title );
    }

    /**
     * Debug Message
     *
     * @param $msg
     * @param null $title
     * @return Logger
     */
    public function debug( $msg, $title = null )
    {
        //  Return
        return $this->write( $msg, 'D', $title );
    }

    /**
     * Info Message
     *
     * @param $msg
     * @param null $title
     * @return Logger
     */
    public function info( $msg, $title = null )
    {
        //  Return
        return $this->write( $msg, 'I', $title );
    }

    /**
     * Log Message
     *
     * @param $msg
     * @param null $title
     * @return Logger
     */
    public function log( $msg, $title = null )
    {
        //  Return
        return $this->write( $msg, 'L', $title );
    }

    /**
     * Write Message
     *
     * @param string $msg
     * @param string $type
     * @param null $title
     * @return $this
     */
    public function write( $msg, $type, $title = null )
    {
        //  Store Last Message
        $this->_last_msg = $msg;

        //  Store Last Message Type
        $this->_last_msg_type = $type;

        //  Check
        if( ( $data = $this->prepare_var( $msg ) ) !== false )
        {
            //  Line
            $line = "[" . date( 'Y-m-d H:i:s' ) . "] [{$type}]" . ( $title ? '.' . $title : '' ) . " :: " . ( $data instanceof \Exception ? ( $data->getMessage() . ' on file ' . $data->getFile() . ' at line ' . $data->getLine() /*. "\n" . $data->getTraceAsString()*/ ) : ( is_object( $data ) || is_array( $data ) ? print_r( $data, true ) : $data ) ) . "\n";

            //  Write
            $this->_fn && fwrite( $this->_fn, $line, strlen( $line ) );
        }

        //  Return
        return $this;
    }

    /**
     * Prepare Var for Print
     *
     * @param mixed $var
     * @return mixed
     */
    private function prepare_var( $var )
    {
        //  Data
        $data = $var;

        //  Check for Bool
        if( is_bool( $data ) )
        {
            //  Convert to String
            $data = ( $data === true ? 'true' : 'false' );
        }

        //  Check for Array
        if( is_array( $data ) && sizeof( $data ) < 1 )  $data = false;

        //  Check
        if( !is_numeric( $var ) && !is_array( $data ) && !is_object( $data ) && strlen( $data ) < 1 )   $data = false;

        //  Return
        return $data;
    }

    /**
     * Get Last Message
     *
     * @return string
     */
    public function lastMessage()
    {
        //  Return
        return $this->_last_msg;
    }

    /**
     * Get Last Message Type
     *
     * @return string
     */
    public function lastMessageType()
    {
        //  Return
        return $this->_last_msg_type;
    }
}