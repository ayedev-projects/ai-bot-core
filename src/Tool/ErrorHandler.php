<?php namespace Ayedev\Bot\Tool;

use Ayedev\Bot\Messenger\Exception\ApiException;

class ErrorHandler
{
    /** @var array $ignore */
    private $ignore = [];

    /** @var callable $custom_handler */
    private $custom_handler = null;

    /** @var array $callables */
    private $callables = [];


    /**
     * ErrorHandler constructor.
     */
    public function __construct()
    {
        //  Set Error Handler
        set_error_handler( array( $this, '_capture_error' ) );

        //  Set Error Handler
        set_exception_handler( array( $this, '_capture_exception' ) );

        //  Register Shutdown Function
        register_shutdown_function( array( $this, '_shutdown_error' ) );

        //  Return
        return $this;
    }

    /**
     * Add Ignore Class
     *
     * @param $class
     * @return $this
     */
    public function addIgnore( $class )
    {
        //  Append
        $this->ignore[] = $class;

        //  Return
        return $this;
    }

    /**
     * Check is ignoring
     *
     * @param $class
     * @return bool
     */
    public function isIgnoring( $class )
    {
        //  Return
        return ( in_array( get_class( $class ), $this->ignore ) );
    }

    /**
     * Add Callable
     * 
     * @param callable $callable
     * @return $this
     */
    public function addCallable( callable $callable )
    {
        //  Store
        $this->callables[] = $callable;

        //  Return
        return $this;
    }

    /**
     * Set Custom Handler
     *
     * @param callable $handler
     * @return $this
     */
    public function setCustomHandler( callable $handler )
    {
        //  Store
        $this->custom_handler = $handler;

        //  Return
        return $this;
    }

    /**
     * Capture Error
     *
     * @param $code
     * @param $error
     * @param $file
     * @param $line
     * @param null $exception
     * @return $this
     */
    public function _capture_error( $code, $error, $file, $line, $exception = null )
    {
        //  Log Error
        bot_logger() && bot_logger()->error( ( is_array( $error ) ? $error['message'] : $error ) . ( is_array( $error ) ? ' on file ' . $error['file'] . '@' . $error['line'] : ( $file ? ' on file ' . $file . '@' . $line : '' ) ), 'ERROR_CAPTURED' );
        bot_logger() && $exception && isset( $exception['e'] ) && bot_logger()->error( $exception['e']->getMessage() . ' on file ' . $exception['e']->getFile() . '@' . $exception['e']->getLine(), 'ERROR_CAPTURED_EXCEPTION' );

        //  Trigger Callables
        $exception && isset( $exception['e'] ) && $this->triggerCallables( $exception['e'] );
        !$exception && $this->triggerCallables( is_array( $error ) ? $error : array( 'code' => $code, 'message' => $error, 'file' => $file, 'line' => $line ) );

        //  Print the Error Display
        return ( $exception && is_array( $exception ) && isset( $exception['e'] ) ? $this->errorDisplay( $exception['e'] ) : $this->errorDisplay( $error, $code, $file, $line ) );
    }

    /**
     * Capture Exception
     *
     * @internal true
     * @param $exception
     * @return $this
     */
    public function _capture_exception( $exception )
    {
        //  Log Error
        bot_logger() && bot_logger()->error( $exception, 'EXCEPTION_CAPTURED' );

        //  Trigger Callables
        $this->triggerCallables( $exception );

        //  Check
        if( $this->isIgnoring( $exception ) )
        {
            //  Mark Ignore
            global $ignore_error;
            $ignore_error = true;

            //  Check
            if( $this->custom_handler )
            {
                //  Call
                $result = call_user_func_array( $this->custom_handler, array( $exception ) );

                //  Check
                if( $result )   return $result;
            }

            //  Return
            return $this;
        }

        //  Print the Error Display
        return $this->errorDisplay( $exception );
    }

    /**
     * Trigger Callables
     * 
     * @return $this
     */
    private function triggerCallables( ...$args )
    {
        //  Check
        if( $this->callables && sizeof( $this->callables ) > 0 )
        {
            //  Loop Each
            foreach( $this->callables as $callable )
            {
                //  Run Callback
                call_user_func_array( $callable, $args );
            }
        }

        //  Return
        return $this;
    }

    /**
     * Capture Shutdown Error
     *
     * @internal true
     * @return $this
     */
    public function _shutdown_error()
    {
        //  Global
        global $ignore_error;

        //  Check
        if( ( $error = error_get_last() ) && $ignore_error !== true && !( is_array( $error ) && stripos( $error['message'], '$HTTP_RAW_POST_DATA' ) > -1 ) )
        {
            //  Log Error
            bot_logger() && bot_logger()->error( $error instanceof \Exception || $error instanceof \Error ? $error : ( ( is_array( $error ) ? $error['message'] : $error ) . ( is_array( $error ) ? ' on file ' . $error['file'] . '@' . $error['line'] : '' ) ), 'SHUTDOWN_ERROR' );

            //  Trigger Callables
            ( $error instanceof \Exception || $error instanceof \Error ) && $this->triggerCallables( $error );

            //  Print the Error Display
            return $this->errorDisplay( $error );
        }

        //  Return
        return $this;
    }

    /**
     * Error Display
     *
     * @param string|\Exception|ApiException|\Error $error
     * @param null $code
     * @param null $file
     * @param null $line
     * @param string $error_code
     * @param bool $clear
     * @return $this
     */
    public function errorDisplay( $error, $code = null, $file = null, $line = null, $error_code = null, $clear = true )
    {
        //  Check
        if( $clear )
        {
            //  Level
            $level = ob_get_level();

            //  Loop
            for( $i = 0; $i < $level; $i++ )
            {
                //  Clear
                @ob_end_clean();
            }
        }

        //  Check
        if( is_array( $error ) && isset( $error['file'] ) )
        {
            //  Extract Data
            $line = $error['line'];
            $file = $error['file'];
            $error = $error['message'];
        }
        ?>
        <div style="padding:20px;background:#fff6f6;border:2px solid #ffe0e0;margin:30px;word-wrap:break-word;line-height:1.6em;font-size:16px;font-family:Arial, sans-serif;">
            <h2 style="margin-top:0;margin-bottom:25px;"><?php echo ( $error_code ?: 'Application Error' ); ?></h2>

            <p style="margin-bottom:0;">
                <?php if( $error instanceof \Exception || $error instanceof \Error ) { ?>
                    <?php if( $error->getCode() > 0 ) { ?>
                        <strong>Code:</strong> <?php echo $error->getCode(); ?><br/>
                    <?php } ?>
                    <strong>Type:</strong> <?php echo get_class( $error ); ?><br/>
                    <?php if( $error instanceof ApiException && $error->hasHeadError() && ( $graphHeadError = $error->getHeadError() ) ) { ?>
                        <strong>Request Error:</strong> <pre><?php echo $graphHeadError; ?></pre>
                    <?php } ?>
                    <?php if( $error instanceof ApiException && $error->hasResponse() && ( $graphError = $error->getResponse() ) ) { ?>
                        <strong>JSON Data:</strong> <pre><?php echo ( is_array( $graphError ) ? print_r( $graphError, true ) : $error->getResponse() ); ?></pre>
                    <?php } ?>
                    <strong>Message:</strong> <?php echo $error->getMessage(); ?><br/>
                    <strong>File:</strong> <?php echo $error->getFile() . ':' . $error->getLine(); ?><br/>
                    <strong>Stack Trace:</strong> <?php echo implode( '<br/>', explode( "\n", $error->getTraceAsString() ) ); ?>
                <?php } else { ?>
                    <strong>Code:</strong> <?php echo (string)$code; ?><br/>
                    <strong>Message:</strong> <?php echo ( is_array( $error ) ? print_r( $error, true ) : (string)$error ); ?><br/>
                    <?php if( $file && $line ) { ?>
                    <strong>File:</strong> <?php echo $file . ':' . $line; ?><br/>
                    <?php } ?>
                <?php } ?>
            </p>
        </div>
        <?php
        //  Check
        if( $clear )
        {
            //  Exit
            exit;
        }

        //  Return
        return $this;
    }

    /**
     * To String
     *
     * @return string
     */
    public function __toString()
    {
        //  Return
        return 'Error!';
    }
}