<?php namespace Ayedev\Bot\AI\Traits;

use Ayedev\Bot\Messenger\IFace\EventInterface;

trait AIFulFillmentTrait
{
    /** @var array $_fulfillments */
    protected $_fulfillments = array();


    /**
     * Message Fillers
     *
     * @return array
     */
    abstract protected function messageFillers();

    /**
     * Get Filler Regex
     *
     * @return string
     */
    protected function getFillerRegex()
    {
        //  Return
        return '/\$([a-zA-Z0-9\_]+)/i';
    }

    /**
     * Get Resolved List
     * @return array
     */
    public function getResolvedList()
    {
        //  Return
        return $this->_fulfillments;
    }

    /**
     * Fulfill Message
     *
     * @param EventInterface $aiEvent
     * @param string $message
     * @param bool $cache
     * @return mixed
     */
    public function fulFillMessage( EventInterface $aiEvent, $message, $cache = true )
    {
        //  Check
        if( !is_string( $message ) )    return $message;

        //  Clear Fulfillments
        if( !$cache )   $this->_fulfillments = array();

        //  Search
        $matches = null;
        preg_match_all( $this->getFillerRegex(), $message, $matches, PREG_SET_ORDER );

        //  Check
        if( $matches )
        {
            //  Get Mappings
            $mappings = $this->messageFillers();

            //  Loop Each
            foreach( $matches as $match )
            {
                //  Search and Replacer
                $search = $match[0];
                $replacer = null;

                //  Check
                if( $cache && isset( $this->_fulfillments[$search] ) )
                {
                    //  Get Replacer
                    $replacer = $this->_fulfillments[$search];
                }
                else
                {
                    //  Get Replacer
                    $replacer = ( isset( $mappings[$match[1]] ) ? $mappings[$match[1]] : null );

                    //  Check
                    if( $replacer )
                    {
                        //  Check
                        if( is_callable( $replacer ) )
                        {
                            //  Get Replacer
                            $replacer = call_user_func_array( $replacer, array( $aiEvent, $message ) );
                        }

                        //  Store Cache
                        if( $cache )    $this->_fulfillments[$search] = $replacer;
                    }
                }

                //  Change
                if( $replacer ) $message = str_ireplace( $search, $replacer, $message );
            }
        }

        //  Return
        return $message;
    }
}