<?php namespace Ayedev\Bot\AI\Impl;

use Ayedev\Bot\AI\IFace\AIFulfillmentInterface;
use Ayedev\Bot\AI\IFace\GeneratorFactoryInterface;
use Ayedev\Bot\Messenger\IFace\EventInterface;

abstract class GeneratorFactory implements GeneratorFactoryInterface
{
    /** @var array $_messages */
    protected $_messages = array();

    /** @var AIFulfillmentInterface $_filler */
    protected $_filler;

    /** @var bool $_ignore_platform */
    protected $_ignore_platform;


    /**
     * Generator Factory constructor.
     *
     * @param EventInterface $event
     * @param AIFulfillmentInterface $filler
     * @param bool $ignore_platform
     */
    public function __construct( AIFulfillmentInterface $filler = null, EventInterface $event = null, $ignore_platform = false )
    {
        //  Store Filler
        if( $filler )   $this->setFiller( $filler );

        //  Check
        if( $event )    $this->parseMessages( $event );

        //  Store
        $this->_ignore_platform = $ignore_platform;
    }


    /**
     * Get Messages
     *
     * @return array
     */
    public function getMessages()
    {
        //  Return
        return $this->_messages;
    }

    /**
     * Parse Messages
     *
     * @param EventInterface $event
     * @param bool $force
     * @return $this
     */
    public function parseMessages( EventInterface $event, $force = false )
    {
        //  Check
        if( ( $force || ( !$this->_messages || sizeof( $this->_messages ) < 1 ) ) && $event->hasResponse() )
        {
            //  Messages
            $messages = array();

            //  Raw Messages
            $rawMessages = $event->getResponse()->getMessages();

            //  Loop Each Messages
            foreach( $rawMessages as $messageData )
            {
                //  Verify Platform
                if( $this->_ignore_platform || ( $this->extractPlatformValue( $messageData ) === true || mManager()->getPlatform() == $this->extractPlatformValue( $messageData ) ) )
                {
                    //  Parse Message
                    $message = $this->parseMessage( $event, $messageData );

                    //  Check
                    if( $message )  $messages[] = $message;
                }
            }

            //  Store
            $this->_messages = $messages;
        }

        //  Return
        return $this;
    }

    /**
     * Extract Platform Value
     *
     * @param $source
     * @return bool|mixed
     */
    public function extractPlatformValue( $source )
    {
        //  Return
        return ( is_array( $source ) && isset( $source['platform'] ) ? $source['platform'] : false );
    }

    /**
     * Parse Messages
     *
     * @param EventInterface $event
     * @param array $messageData
     * @return mixed
     */
    abstract protected function parseMessage( EventInterface $event, array $messageData );


    /**
     * @inheritdoc
     */
    public function setFiller( AIFulfillmentInterface $filler )
    {
        //  Store
        $this->_filler = $filler;

        //  Return
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getFiller()
    {
        //  Return
        return $this->_filler;
    }

    /**
     * @inheritdoc
     */
    public function hasFiller()
    {
        //  Return
        return ( $this->_filler && !is_null( $this->_filler ) );
    }

    /**
     * Fill the Placeholder Texts
     *
     * @param EventInterface $event
     * @param $text
     * @return mixed
     */
    public function fill( EventInterface $event, $text )
    {
        //  Return
        return ( $this->hasFiller() ? $this->getFiller()->fulFillMessage( $event, $text ) : $text );
    }
}