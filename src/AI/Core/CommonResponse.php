<?php namespace Ayedev\Bot\AI\Core;

use Ayedev\Bot\AI\IFace\AIFulfillmentInterface;
use Ayedev\Bot\Messenger\IFace\EventInterface;

class CommonResponse extends AIResponse
{
    /**
     * @inheritdoc
     */
    public function parseResponse()
    {
        //  Set Success
        $this->_success = true;
    }


    /**
     * @inheritdoc
     */
    public function isEmpty()
    {
        //  Return
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getSpeech()
    {
        //  Return
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getMessages()
    {
        //  Return
        return array();
    }

    /**
     * @inheritdoc
     */
    public function makeMessages( AIFulfillmentInterface $filler = null, EventInterface $event = null, $ignore_platform = false )
    {
        //  Nothing
    }

    /**
     * @inheritdoc
     */
    public function getAction()
    {
        //  Return
        return null;
    }

    /**
     * @inheritdoc
     */
    public function actionCompleted()
    {
        //  Return
        return false;
    }

    /**
     * @inheritdoc
     */
    public function hasParam( $key )
    {
        //  Return
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getParam( $key, $ignore_confidence = false )
    {
        //  Return
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getParams( $only = array(), $except = array(), $original = true )
    {
        //  Return
        return array();
    }

    /**
     * @inheritdoc
     */
    public function hasContext( $context )
    {
        //  Return
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getContextParam( $context, $key, $def = null )
    {
        //  Return
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getContextParams( $context, $only = array(), $except = array(), $original = true )
    {
        //  Return
        return null;
    }

    /**
     * @inheritdoc
     */
    public function makeContext( $context, $only = array(), $except = array() )
    {
        //  Return
        return array();
    }

    /**
     * @inheritdoc
     */
    public function invalidateContext( $session_id, $context, $only = array(), $except = array(), $params = array() )
    {
        //  Return
        return null;
    }

    /**
     * @inheritdoc
     */
    public function invalidateContextFromIntent( $session_id, $params = array() )
    {
        //  Return
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getContext( $context )
    {
        //  Return
        return array();
    }

    /**
     * @inheritdoc
     */
    public function getContexts()
    {
        //  Return
        return array();
    }

    /**
     * Get Intent ID
     *
     * @return string
     */
    public function getIntentID()
    {
        //  Return
        return null;
    }

    /**
     * Get Intent Info
     *
     * @return CommonResponse
     */
    public function getIntentInfo()
    {
        //  Return
        return null;
    }

    /**
     * Get to be affected contexts
     *
     * @return array
     */
    public function toBeAffectedContexts()
    {
        //  Return
        return array();
    }
}