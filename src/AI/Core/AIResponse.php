<?php namespace Ayedev\Bot\AI\Core;

use Ayedev\Bot\AI\IFace\AIResponseInterface;

abstract class AIResponse implements AIResponseInterface
{
    /** Minimum Confidence Level */
    const CONFIDENCE_LEVEL_THRESHOLD = 0.65;

    /** @var bool $_success */
    protected $_success = false;

    /** @var float $_confidence */
    protected $_confidence = 0;

    /** @var array $_response */
    protected $_response;


    /**
     * AIResponse constructor.
     *
     * @param $response
     */
    public function __construct( $response )
    {
        //  Store Response
        $this->_response = $response;

        //  Run Parse Response
        $this->parseResponse();
    }

    /**
     * Check for Success
     *
     * @return bool
     */
    public function isSuccess()
    {
        //  Return
        return $this->_success;
    }

    /**
     * Check for Error
     *
     * @return bool
     */
    public function isError()
    {
        //  Return
        return !$this->isSuccess();
    }

    /**
     * @inheritdoc
     */
    public function isConfident()
    {
        //  Return
        return ( $this->getConfidenceLevel() >= static::CONFIDENCE_LEVEL_THRESHOLD );
    }

    /**
     * @inheritdoc
     */
    public function getConfidenceLevel()
    {
        //  Return
        return $this->_confidence;
    }

    /**
     * Get Response
     *
     * @return array
     */
    public function getResponse()
    {
        //  Return
        return $this->_response;
    }

    /**
     * @inheritdoc
     */
    public static function make( $data )
    {
        //  Return
        return new static( $data );
    }

    /**
     * Convert to String
     *
     * @return string
     */
    public function __toString()
    {
        //  Return
        return print_r( $this->getResponse(), true );
    }
}