<?php namespace Ayedev\Bot\AI\Core;

use Ayedev\Bot\AI\IFace\AIEventInterface;
use Ayedev\Bot\AI\IFace\AIFulfillmentInterface;
use Ayedev\Bot\AI\IFace\AIInterface;
use Ayedev\Bot\Messenger\Core\ApiCall;
use Ayedev\Bot\Messenger\IFace\EventInterface;
use Ayedev\Bot\Messenger\Impl\AbstractSkeleton;
use Ayedev\Bot\Messenger\Event\ChatEvent;

abstract class AI extends AbstractSkeleton implements AIInterface
{
    /**
     * API Base URI.
     */
    const API_BASE_URI = null;

    /**
     * API Version
     */
    const API_VERSION = 0;

    /**
     * API Version Endpoint
     */
    const API_VERSION_ENDPOINT = null;


    /** @var string $_lang */
    protected $_lang = 'en';

    /** @var string $_token */
    protected $_token;

    /** @var ApiCall $_apiCall */
    protected $_apiCall;

    /** @var AIEventInterface $_callback */
    protected $_callback;

    /** @var AIFulfillmentInterface $_filler */
    protected $_filler;

    /** @var string $_developerToken */
    protected $_developerToken;

    /** @var string $_staticMessageEntityKey */
    protected $_staticMessageEntityKey;

    /** @var string|callable $_defaultResponse */
    protected $_defaultResponse;


    /**
     * AI constructor.
     *
     * @param $api_token
     * @param ApiCall|null $apiCall
     */
    public function __construct( $api_token = null, ApiCall $apiCall = null )
    {
        //  Store Api Call
        $this->_apiCall = $apiCall ?: $this->createApiCall();

        //  Set Token
        if( $api_token )    $this->setToken( $api_token );
    }

    /**
     * @inheritdoc
     */
    public function createApiCall()
    {
        //  Return
        return ( new ApiCall( null, null, static::API_BASE_URI, static::API_VERSION_ENDPOINT ) )->disableUseAccessToken();
    }

    /**
     * Get API Call
     *
     * @return ApiCall
     */
    public function getAPICall()
    {
        //  Return
        return $this->_apiCall;
    }

    /**
     * Set Callback
     *
     * @param AIEventInterface $callback
     * @return $this
     */
    public function setCallback( AIEventInterface $callback )
    {
        //  Store
        $this->_callback = $callback;

        //  Return
        return $this;
    }

    /**
     * Get Callback
     *
     * @return AIEventInterface
     */
    public function getCallback()
    {
        //  Return
        return $this->_callback;
    }

    /**
     * Set the Token
     *
     * @param $token
     * @return $this
     */
    public function setToken( $token )
    {
        //  Store
        $this->_token = $token;

        //  Return
        return $this;
    }

    /**
     * Get Token
     *
     * @return string
     */
    public function getToken()
    {
        //  Return
        return $this->_token;
    }

    /**
     * With Token (ALIAS)
     *
     * @param $token
     * @return AI
     */
    public static function withToken( $token )
    {
        //  Return
        return self::call( 'setToken', $token );
    }


    /**
     * @inheritdoc
     */
    public function getHeaders( $token = null )
    {
        //  Return
        return array(
            'X-AYEDEV-BOT-REQUEST' => '1',
            'X-AYEDEV-BOT-VERSION' => AYEDEV_BOT_LIB_VERSION,
            'X-AYEDEV-BOT-SOURCE' => 'ai',
            'X-AYEDEV-BOT-PLATFORM' => 'php',
            'Accept' => 'application/json',
            'Content-Type' => 'application/json; charset=utf-8',
            'Authorization' => 'Bearer ' . ( $token ?: $this->getToken() )
        );
    }


    /**
     * Set Filler
     *
     * @param AIFulfillmentInterface $filler
     * @return $this
     */
    public function setFiller( AIFulfillmentInterface $filler )
    {
        //  Store
        $this->_filler = $filler;

        //  Return
        return $this;
    }

    /**
     * Get Filler
     *
     * @return AIFulfillmentInterface
     */
    public function getFiller()
    {
        //  Return
        return $this->_filler;
    }


    /**
     * Set Language
     *
     * @param $lang
     * @return $this
     */
    public function setLang( $lang )
    {
        //  Store
        $this->_lang = $lang;

        //  Return
        return $this;
    }

    /**
     * Get Language
     *
     * @return string
     */
    public function getLang()
    {
        //  Return
        return $this->_lang;
    }


    /**
     * @inheritdoc
     */
    public function sendResponse( EventInterface $event, $params = [] )
    {
        //  Check
        if( $event->isDefaultPrevented() )    return null;

        //  Response
        $response = $this->converseText( $event->getSessionID(), $event->toString(), $params );

        //  Check
        if( $this->_callback && method_exists( $this->_callback, 'manipulateResponse' ) )    $response = $this->_callback->manipulateResponse( $response, $event );

        //  Attach Response
        $event->attachResponse( $response );

        //  Return
        return $this->handleConverse( $event );
    }

    /**
     * Handle AI Converse
     *
     * @param EventInterface $event
     * @return EventInterface
     */
    public function handleConverse( EventInterface $event )
    {
        //  Save Contexts
        if( method_exists( $event, 'setContexts' ) )    $event->setContexts( $event->getResponse()->getContexts() );

        //  Process
        $process = true;

        //  Check for Action
        if( $event->getResponse()->getAction() )
        {
            //  Dispatch Action
            mManager()->dispatch( 'ai_action.' . ( $event->getResponse()->actionCompleted() ? '' : 'raw.' ) . $event->getResponse()->getAction(), $event );
            mManager()->dispatch( 'ai_action.' . ( $event->getResponse()->actionCompleted() ? '' : 'raw.' ) . '__global__', $event );

            //  Run Callback
            if( $this->_callback )  $process = $this->_callback->onAction( $event );
        }

        //  Dispatch
        mManager()->dispatch( '__global_ai__', $event );

        //  On Message
        if( $this->_callback )  $this->_callback->onMsg( $event );

        //  Check
        if( $event->isDefaultPrevented() )    $process = false;

        //  Check
        if( $process !== false )
        {
            //  Trigger ChatEvent
            ChatEvent::dispatchRequestEvent( $event, 'AI' );
        }

        //  Check
        if( $this->_callback && method_exists( $this->_callback, 'verifyAIReply' ) )    $process = $this->_callback->verifyAIReply( $event );

        //  Check for Message
        if( $process !== false && $event )
        {
            //  Get Messages
            $messages = $event->getResponse()->makeMessages( $this->_filler, $event );

            //  Check
            if( $this->_callback && method_exists( $this->_callback, 'manipulateMessages' ) )   $messages = $this->_callback->manipulateMessages( $messages, $event );

            //  Check
            if( $messages && is_array( $messages ) && sizeof( $messages ) > 0 )
            {
                //  Loop Each
                foreach( $messages as $message )
                {
                    //  Check
                    if( $this->_callback && method_exists( $this->_callback, 'manipulateMessage' ) )    $this->_callback->manipulateMessage( $message, $event );

                    //  Send Reply
                    $event->reply( $message );
                }
            }
            else
            {
                //  Get Default Response
                $defaultResponse = $this->getDefaultResponse( $event, $this->_filler );

                //  Check
                if( $defaultResponse )
                {
                    //  Perform Reply
                    $event->reply( $defaultResponse );
                }
            }
        }

        //  Dispatch
        mManager()->dispatch( '__global_ai_after__', $event );

        //  Check for Action
        if( $event->getResponse()->getAction() )
        {
            //  Run Callback
            if( $this->_callback && method_exists( $this->_callback, 'onActionAfter' ) )  $this->_callback->onActionAfter( $event );

            //  Dispatch Action
            mManager()->dispatch( 'ai_action.after.' . ( $event->getResponse()->actionCompleted() ? '' : 'raw.' ) . $event->getResponse()->getAction(), $event );
            mManager()->dispatch( 'ai_action.after.' . ( $event->getResponse()->actionCompleted() ? '' : 'raw.' ) . '__global__', $event );
        }

        //  Return
        return $event;
    }


    /**
     * Read Static Messages
     *
     * @param bool $force
     * @return array
     */
    public function readStaticMessages( $force = false )
    {
        //  Messages
        $messages = array();

        //  Cache File
        $cache_file = $this->cacheFile();

        //  File to Load
        $file_to_load = $cache_file;

        //  Check
        if( $force || !file_exists( $cache_file ) )
        {
            //  Set
            $file_to_load = false;
        }

        //  Check
        if( !$file_to_load )
        {
            //  Extract Messages
            $messages = $this->extractStaticMessages();

            //  Check
            if( !file_exists( $cacheDir = dirname( $cache_file ) ) )
            {
                //  Make Dir
                mkdir( $cacheDir, 0775, true );
            }

            //  Save Cache
            file_put_contents( $cache_file, json_encode( $messages ) );
        }
        else
        {
            //  Read from File
            $messages = json_decode( file_get_contents( $file_to_load ), true );
        }

        //  Return
        return $messages;
    }

    /**
     * Extract Static Messages
     *
     * @return array
     */
    protected function extractStaticMessages()
    {
        //  Return
        return array();
    }

    /**
     * Set Developer Token
     *
     * @param $token
     * @return $this
     */
    public function setDeveloperToken( $token )
    {
        //  Store
        $this->_developerToken = $token;

        //  Return
        return $this;
    }

    /**
     * Get Developer Token
     *
     * @return string
     */
    public function getDeveloperToken()
    {
        //  Return
        return $this->_developerToken;
    }

    /**
     * Set Static Message Entity Key
     *
     * @param $key
     * @return $this
     */
    public function setStaticMessageEntityKey( $key )
    {
        //  Store
        $this->_staticMessageEntityKey = $key;

        //  Return
        return $this;
    }


    /**
     * @inheritdoc
     */
    public function cacheFile( $name = null )
    {
        //  Return
        return mManager()->getDataPath( md5( $name ?: get_called_class() . date( 'Y-m-d H:00' ) ) . '.json', mManager()->getWebhook()->getCacheDir() );
    }

    /**
     * @inheritdoc
     */
    public function setDefaultResponse( $resolver )
    {
        //  Store
        $this->_defaultResponse = $resolver;
        
        //  Return
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getDefaultResponse( ...$args )
    {
        //  Check
        if( $this->_defaultResponse )
        {
            //  Check
            if( is_callable( $this->_defaultResponse ) )
            {
                //  Return
                return call_user_func_array( $this->_defaultResponse, $args );
            }
            else if( is_string( $this->_defaultResponse ) )
            {
                //  Return
                return $this->_defaultResponse;
            }
        }

        //  Return
        return null;
    }
}