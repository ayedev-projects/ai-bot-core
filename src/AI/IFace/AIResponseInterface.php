<?php namespace Ayedev\Bot\AI\IFace;

use Ayedev\Bot\AI\Core\CommonResponse;
use Ayedev\Bot\Messenger\IFace\EventInterface;

interface AIResponseInterface
{
    /**
     * Parse Response
     */
    public function parseResponse();

    /**
     * Check Empty
     *
     * @return bool
     */
    public function isEmpty();

    /**
     * Get Speech
     *
     * @return string
     */
    public function getSpeech();

    /**
     * Get Messages
     *
     * @return array
     */
    public function getMessages();

    /**
     * Make Packaged Messages
     *
     * @param AIFulfillmentInterface $filler
     * @param EventInterface $event
     * @param bool $ignore_platform
     * @return array
     */
    public function makeMessages( AIFulfillmentInterface $filler = null, EventInterface $event = null, $ignore_platform = false );

    /**
     * Get Action
     *
     * @return string
     */
    public function getAction();

    /**
     * Check Action Completed
     *
     * @return bool
     */
    public function actionCompleted();

    /**
     * Get Confidence Level
     *
     * @return array
     */
    public function getConfidenceLevel();

    /**
     * Check has Parameter
     *
     * @param $key
     * @return bool
     */
    public function hasParam( $key );

    /**
     * Get Parameter Value
     *
     * @param $key
     * @param bool $ignore_confidence
     * @return mixed
     */
    public function getParam( $key, $ignore_confidence = false );

    /**
     * Get Parameters
     *
     * @param string|array|null $only
     * @param string|array|null $except
     * @param bool $original
     * @return array
     */
    public function getParams( $only = array(), $except = array(), $original = true );

    /**
     * Check has Context
     *
     * @param $context
     * @return bool
     */
    public function hasContext( $context );

    /**
     * Get Context Parameter
     *
     * @param $context
     * @param $key
     * @param null $def
     * @return mixed
     */
    public function getContextParam( $context, $key, $def = null );

    /**
     * Get Context Parameters
     *
     * @param $context
     * @param string|array|null $only
     * @param string|array|null $except
     * @param bool $original
     * @return mixed
     */
    public function getContextParams( $context, $only = array(), $except = array(), $original = true );

    /**
     * Make New Context
     *
     * @param $context
     * @param array $only
     * @param array $except
     * @return mixed
     */
    public function makeContext( $context, $only = array(), $except = array() );

    /**
     * Invalidate Context
     *
     * @param $session_id
     * @param $context
     * @param array $only
     * @param array $except
     * @param array $params
     * @return mixed
     */
    public function invalidateContext( $session_id, $context, $only = array(), $except = array(), $params = array() );

    /**
     * Get Contexts
     *
     * @param $context
     * @return array
     */
    public function getContext( $context );

    /**
     * Get Contexts
     *
     * @return array
     */
    public function getContexts();

    /**
     * Check is confident
     *
     * @return array
     */
    public function isConfident();

    /**
     * Get Intent ID
     *
     * @return string
     */
    public function getIntentID();

    /**
     * Get Intent Info
     *
     * @return CommonResponse
     */
    public function getIntentInfo();

    /**
     * Get to be affected contexts
     *
     * @return array
     */
    public function toBeAffectedContexts();

    /**
     * Make Response
     *
     * @param $data
     * @return AIResponseInterface
     */
    public static function make( $data );
}