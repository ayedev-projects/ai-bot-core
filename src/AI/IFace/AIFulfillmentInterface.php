<?php namespace Ayedev\Bot\AI\IFace;

use Ayedev\Bot\Messenger\IFace\EventInterface;

interface AIFulfillmentInterface
{
    /**
     * Get Resolved List
     *
     * @return mixed
     */
    public function getResolvedList();

    /**
     * Fulfill Message
     *
     * @param EventInterface $event
     * @param $message
     * @param bool $cache
     * @return mixed
     */
    public function fulFillMessage( EventInterface $event, $message, $cache = true );
}