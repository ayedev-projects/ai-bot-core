<?php namespace Ayedev\Bot\AI\IFace;

interface GeneratorFactoryInterface
{
    /**
     * Get Generated Messages
     *
     * @return array
     */
    public function getMessages();

    /**
     * Set Filler
     *
     * @param AIFulfillmentInterface $filler
     * @return $this
     */
    public function setFiller( AIFulfillmentInterface $filler );

    /**
     * Get Filler
     *
     * @return AIFulfillmentInterface
     */
    public function getFiller();

    /**
     * Has Filler
     *
     * @return bool
     */
    public function hasFiller();
}