<?php namespace Ayedev\Bot\AI\IFace;

use Ayedev\Bot\AI\Core\CommonResponse;
use Ayedev\Bot\AI\Core\AIResponse;
use Ayedev\Bot\Messenger\Core\ApiCall;
use Ayedev\Bot\Messenger\IFace\EventInterface;

interface AIInterface
{
    /**
     * Create ApiCall
     *
     * @return ApiCall
     */
    public function createApiCall();

    /**
     * Get ApiCall
     *
     * @return ApiCall
     */
    public function getApiCall();

    /**
     * Set Lang
     *
     * @param string $lang
     * @return $this
     */
    public function setLang( $lang );

    /**
     * Get Lang
     *
     * @return string
     */
    public function getLang();

    /**
     * Set Callback
     *
     * @param AIEventInterface $callback
     * @return $this
     */
    public function setCallback( AIEventInterface $callback );

    /**
     * Get Callback
     *
     * @return AIEventInterface
     */
    public function getCallback();

    /**
     * Set Filler
     *
     * @param AIFulfillmentInterface $filler
     * @return $this
     */
    public function setFiller( AIFulfillmentInterface $filler );

    /**
     * Get Filler
     *
     * @return AIFulfillmentInterface
     */
    public function getFiller();

    /**
     * Set Token
     *
     * @param string $token
     * @return $this
     */
    public function setToken( $token );

    /**
     * Get Token
     *
     * @return string
     */
    public function getToken();

    /**
     * Get Headers for Request
     * 
     * @param string|null $token
     * @return array
     */
    public function getHeaders( $token = null );

    /**
     * Set Developer Token
     *
     * @param string $token
     * @return $this
     */
    public function setDeveloperToken( $token );

    /**
     * Get Developer Token
     *
     * @return string
     */
    public function getDeveloperToken();

    /**
     * Read Static Messages
     *
     * @param bool $force
     * @return array
     */
    public function readStaticMessages( $force = false );

    /**
     * Set Static Message Entity Key
     *
     * @param string $key
     * @return array
     */
    public function setStaticMessageEntityKey( $key );

    /**
     * Read Intent Mappings
     *
     * @param bool $force
     * @param array $params
     * @return array
     */
    public function readIntentMappings( $force = false, $params = array() );

    /**
     * Get Valid Intent Contexts
     *
     * @param string $intent_id
     * @return array
     */
    public function getValidIntentContexts( $intent_id );

    /**
     * Cache File
     *
     * @param null $name
     * @return string
     */
    public function cacheFile( $name = null );

    /**
     * Get Intent for Text
     *
     * @param $q
     * @param array $params
     * @return AIResponse
     */
    public function readTextIntent( $q, $params = [] );

    /**
     * Converse the Text
     *
     * @param string $session_id
     * @param string $q
     * @param array $params
     * @return AIResponse
     */
    public function converseText( $session_id, $q, $params = [] );

    /**
     * Get Response for Event
     *
     * @param $session_id
     * @param $event_name
     * @param array $event_data
     * @param array $params
     * @return AIResponse
     */
    public function triggerEvent( $session_id, $event_name, $event_data = [], $params = [] );

    /**
     * Get Intent
     *
     * @param $intent_id
     * @param array $params
     * @return CommonResponse
     */
    public function getIntent( $intent_id, $params = [] );

    /**
     * Get Intent
     *
     * @param $session_id
     * @param $event_name
     * @param array $event_data
     * @param array $params1
     * @param array $params2
     * @return AIResponse
     */
    public function getConverseFromEvent( $session_id, $event_name, $event_data = [], $params1 = [], $params2 = [] );

    /**
     * Get Contexts
     *
     * @param $session_id
     * @param array $params
     * @return CommonResponse
     */
    public function getContexts( $session_id, $params = [] );

    /**
     * Get Context
     *
     * @param $session_id
     * @param $context
     * @param array $params
     * @return CommonResponse
     */
    public function getContext( $session_id, $context, $params = [] );

    /**
     * Get Context
     *
     * @param $session_id
     * @param array $context_data
     * @param array $params
     * @return CommonResponse
     */
    public function postContext( $session_id, $context_data, $params = [] );

    /**
     * Clear Contexts
     *
     * @param $session_id
     * @param array $params
     * @return CommonResponse
     */
    public function clearContexts( $session_id, $params = [] );

    /**
     * Delete Context
     *
     * @param $session_id
     * @param $context
     * @param array $params
     * @return CommonResponse
     */
    public function deleteContext( $session_id, $context, $params = [] );

    /**
     * Delete Context
     *
     * @param $session_id
     * @param array $contexts
     * @param array $params
     * @return array
     */
    public function deleteContexts( $session_id, $contexts, $params = [] );

    /**
     * Keep only Contexts
     *
     * @param $session_id
     * @param string|array $contexts
     * @param array $params
     * @param array $params2
     * @return array
     */
    public function onlyContexts( $session_id, $contexts, $params = [], $params2 = [] );

    /**
     * Assign Context
     *
     * @param $session_id
     * @param string $context
     * @param array $parameters
     * @param int $lifespan
     * @param array $params
     * @return CommonResponse
     */
    public function assignContext( $session_id, $context, $parameters = [], $lifespan = 10, $params = [] );

    /**
     * Send Response
     *
     * @param EventInterface $event
     * @param array $params
     * @return EventInterface
     */
    public function sendResponse( EventInterface $event, $params = [] );

    /**
     * Set Default Response
     * 
     * @param string|callable $resolver
     */
    public function setDefaultResponse( $resolver );

    /**
     * Get Default Response
     * 
     * @return mixed
     */
    public function getDefaultResponse( ...$args );
}