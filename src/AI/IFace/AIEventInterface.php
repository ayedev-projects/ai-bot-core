<?php namespace Ayedev\Bot\AI\IFace;

use Ayedev\Bot\Messenger\IFace\EventInterface;

interface AIEventInterface extends AIFulfillmentInterface
{
    /**
     * On Message
     *
     * @param EventInterface $event
     */
    public function onMsg( EventInterface $event );

    /**
     * On Action
     *
     * @param EventInterface $event
     */
    public function onAction( EventInterface $event );

    /**
     * On Error
     *
     * @param EventInterface $event
     */
    public function onError( EventInterface $event );
}